%%

clc 
clear all

load coilData

%%

    i=0;
    figure(i+1)
    subplot(1,2,1)
    eval(strcat('mineDATA = coil',int2str(i), ';') );
    meshc(mineDATA)
    hold on
    % Estimated minemap
    subplot(1,2,2)
    meshc(mineDATA)
    
%%
figure(2)
x=linspace(5,15,100);
y=x';               
[X,Y]=meshgrid(x,y);
x0=10;
y0=10;
z=exp(-((X-x0).^2+(Y-y0).^2)/2);

figure(1)
subplot(1,2,1)
mesh(x,y,z)

% fmincon

% Pick some sample values

hold on
subplot(1,2,2)
mesh(x,y,z)
