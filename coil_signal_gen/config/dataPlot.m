% Author: Isura Ranatunga,
% University of Texas at Arlington
% Dept. of Electrical Engineering
% UT Arlington Research Institute
%
% email address: isura@ieee.org
% Website: isura.me
% Created: 02/06/2015
% Modified: 02/06/2015
%
% Mine detection data collection for HRATC 2015
%

%------------- BEGIN CODE --------------

clc
clear all

load coil0.txt leftCoilChannel1
load coil1.txt leftCoilChannel2
load coil2.txt leftCoilChannel3

load coil3.txt middleCoilChannel1
load coil4.txt middleCoilChannel2
load coil5.txt middleCoilChannel3

load coil6.txt rightCoilChannel1
load coil7.txt rightCoilChannel2
load coil8.txt rightCoilChannel3
load coilZero.txt
coil0 = coil0 - coilZero(1);
coil1 = coil1 - coilZero(2);
coil2 = coil2 - coilZero(3);
coil3 = coil3 - coilZero(4);
coil4 = coil4 - coilZero(5);
coil5 = coil5 - coilZero(6);
coil6 = coil6 - coilZero(7);
coil7 = coil7 - coilZero(8);
coil8 = coil8 - coilZero(9);

load mineLocGrid.txt
load metal1LocGrid.txt
load metal2LocGrid.txt


%%
clc 
close all

for i=0:8
    figure(i+1)
    eval(strcat('surf(coil',strcat( int2str(i), ')') ) );
    eval(strcat('maxVal = max(max(coil',strcat( int2str(i), '));') ) );
    %surf(coil[0])
    hold on
    plot3(mineLocGrid(:,1),mineLocGrid(:,2),maxVal*ones(length(mineLocGrid(:,2))), 'r*')
    hold on
    plot3(metal1LocGrid(:,1),metal1LocGrid(:,2),maxVal*ones(length(metal1LocGrid(:,2))), 'g*')
    hold on
    plot3(metal2LocGrid(:,1),metal2LocGrid(:,2),maxVal*ones(length(metal2LocGrid(:,2))), 'b*')
    %legend('Mine','Metal1', 'Metal2')
end