#!/usr/bin/python

from ConfigParser import ConfigParser
from minemapgenerator import MineMapGenerator, GenerateUsingRealDataset
from numpy import arange
from std_msgs.msg import Bool
from metal_detector_msgs.msg._Coil import Coil
import random, rospy, rospkg
import numpy as np

defaultpath = rospkg.RosPack().get_path("coil_signal_gen") + "/config/"
print defaultpath

class CoilSignalDataCollect(object):

    def __init__(self):
        self.load()

    def load(self,path=defaultpath+"config.ini"):
        configFile = ConfigParser()
        configFile.read(path)
        self.mapWidth = configFile.getfloat("MapDimensions","width")
        self.mapHeight = configFile.getfloat("MapDimensions","height")
        self.resolution = configFile.getfloat("MapDimensions","resolution")

        self.numMines = configFile.getint("Mines","numMines")
        self.randomMines = configFile.getboolean("Mines","RandomMines")

        self.minDistDetection = configFile.getfloat("Mines","DetectionMinDist")
        self.maxDistExplosion = configFile.getfloat("Mines","ExplosionMaxDist")

        self.numCellsX  = self.mapWidth/self.resolution
        self.numCellsY  = self.mapHeight/self.resolution
        self.numCells   = self.numCellsX * self.numCellsY

	self.minesGrid = 0
        self.metals1Grid = 0
	self.metals2Grid = 0
	self.metals1 = 0
	self.metals2 = 0

        self.minesFixedPos = configFile.get("Mines","MinesPositions")
        self.generateMines()
        self.save(path)

    def save(self,path):
        cfile = open(path,"w")

        configFile = ConfigParser()

        configFile.add_section("MapDimensions")
        configFile.set("MapDimensions","width",self.mapWidth)
        configFile.set("MapDimensions","height",self.mapHeight)
        configFile.set("MapDimensions","resolution",self.resolution)

        configFile.add_section("Mines")
        configFile.set("Mines","numMines",self.numMines)
        if(self.randomMines):
            configFile.set("Mines","RandomMines","true")
        else:
            configFile.set("Mines","RandomMines","false")
        self.minesFixedPos = ""
        if not self.randomMines:
            self.minesFixedPos = "|".join( ",".join(str(v) for v in r) for r in self.mines)

        configFile.set("Mines","MinesPositions",self.minesFixedPos)

        configFile.set("Mines","DetectionMinDist",self.minDistDetection)
        configFile.set("Mines","ExplosionMaxDist",self.maxDistExplosion)
        configFile.write(cfile)

        cfile.close()


    def generateMines(self):
        minesPos = self.minesFixedPos
        if minesPos !=  "":
            self.mines = [[float(v) for v in r.split(",")] for r in minesPos.split("|")]
        else:
            self.mines = []

        for i in arange(self.numMines-len(self.mines)):
            self.mines.append([ random.randrange(self.mapWidth)  - self.mapWidth/2., random.randrange(self.mapHeight) - self.mapHeight/2.])

        mWidth, mHeight = self.mapWidth/self.resolution,self.mapHeight/self.resolution
        mines = [ [mWidth/2. + m[0]/self.resolution, mHeight/2. - m[1]/self.resolution] for m in self.mines]

        # Saving mine loc in matrix index form
        self.minesGrid = mines

        metals1 = []
        for i in arange(0.25*self.numMines):
            metals1.append([ random.randrange(self.mapWidth)  - self.mapWidth/2., random.randrange(self.mapHeight) - self.mapHeight/2.])

        metals2 = []
        for i in arange(0.25*self.numMines):
            metals2.append([ random.randrange(self.mapWidth)  - self.mapWidth/2., random.randrange(self.mapHeight) - self.mapHeight/2.])

        self.metals1 = metals1
	self.metals2 = metals2

        metals1 = [ [mWidth/2. + m[0]/self.resolution, mHeight/2. - m[1]/self.resolution] for m in metals1]
        metals2 = [ [mWidth/2. + m[0]/self.resolution, mHeight/2. - m[1]/self.resolution] for m in metals2]

        self.mineMap, self.zeroChannel, self.minesGrid, self.metals1Grid, self.metals2Grid = GenerateUsingRealDataset(mines,metals1,metals2,mWidth,mHeight,True)

        self.mines = [ [m[0]*self.resolution - self.mapWidth/2., -m[1]*self.resolution + self.mapHeight/2.] for m in mines]
        self.numMines = len(self.mines)

    def getMineMap(self):
        return self.mineMap

    def saveMineMap(self):

        np.savetxt(defaultpath+'mineLoc.txt', self.mines)
        np.savetxt(defaultpath+'metal1Loc.txt', self.metals1)
        np.savetxt(defaultpath+'metal2Loc.txt', self.metals2)

        np.savetxt(defaultpath+'mineLocGrid.txt', self.minesGrid)
        np.savetxt(defaultpath+'metal1LocGrid.txt', self.metals1Grid)
        np.savetxt(defaultpath+'metal2LocGrid.txt', self.metals2Grid)

        np.savetxt(defaultpath+'coilZero.txt', self.zeroChannel)

        for i in range(9):
            np.savetxt(defaultpath+'coil' + str(i) + '.txt', self.mineMap[i])

    def getCoilSignals(self, lX, lY, mX, mY, rX, rY):

        leftCoilX = lX/self.resolution + self.numCellsX/2.0
        leftCoilY = lY/self.resolution + self.numCellsY/2.0

        middleCoilX = mX/self.resolution + self.numCellsX/2.0
        middleCoilY = mY/self.resolution + self.numCellsY/2.0

        rightCoilX = rX/self.resolution + self.numCellsX/2.0
        rightCoilY = rY/self.resolution + self.numCellsY/2.0

        coils = [[leftCoilX,leftCoilY]]
        coils.append([middleCoilX, middleCoilY])
        coils.append([rightCoilX, rightCoilY])

        co = 0
        for x,y in coils:
            coil = Coil()
            coil.header.frame_id = "{}_coil".format(["left","middle","right"][co])
            if x <= self.mineMap.shape[2] and x >=0 and y <= self.mineMap.shape[1] and y >= 0:
                for ch in range(3):
                    coil.channel.append(self.mineMap[3*co+ch,y,x] + random.random()*100)
                    coil.zero.append(self.zeroChannel[3*co+ch])

            co += 1

        return coil.channel

if __name__=='__main__':
    
    coilSignalDataCollect = CoilSignalDataCollect()

    print coilSignalDataCollect.getCoilSignals(1,1,2,2,3,3)
    #print coilSignalDataCollect.getMineMap()

    coilSignalDataCollect.saveMineMap()



