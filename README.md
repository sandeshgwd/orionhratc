# HRATC 2015 Codebase for Team Orion
- http://www2.isr.uc.pt/~embedded/events/HRATC2015/Welcome.html

Unzip the "orion" code folder to ~/hratc2015_workspace/src

The instructions below assume you have installed the HRATC 2015 code/simulator at ~/hratc2015_workspace/src

# Procedure to install package assuming Orion code folder is inside ~/hratc2015_workspace/src
- cd ~/hratc2015_workspace/src
- cd ..
- catkin_make

#############
# Expected behavior
#############

The robot will start moving in a spiral away from the center of the field. It will avoide obstacles and mines.

#############
# Components used
#############

The laser scanner, mine sweep arm, and robot base is used. The laser navigation stack is used.

#############
# FOR Simulation runs on scenario1 gazebo world
#############

# Run the Simulator
- roslaunch hratc2015_framework run_simulation_world.launch

# Run Team Orion minedetection support nodes
- roslaunch mine_detector sim_mine_detector.launch

#############
# FOR Simulation runs on empty gazebo world
#############

# Run the Simulator
- roslaunch hratc2015_framework run_simulation_empty.launch

# Run Team Orion minedetection support nodes
- roslaunch mine_detector sim_mine_detector_empty.launch

#############
# FOR Field runs - launch files that do not invoke Gazebo
#############

# Run Team Orion minedetection support nodes 
- roslaunch mine_detector mine_detector_field.launch

#If the corners are being published at a fixed rate then you can publish the corners before with the minefield frame and then run the above launch file
#See to it that ther corners are published along with the launch file, since the corners in the ROSBag are published only twice

#Relaunch the node if it doesnt start moving
#If the sweep height is too low then please increase the parameter height by 0.1 at a time in the file mine_detector/launch/mine_detector.launch 

#############
# FOR Mine detection machine learning using SVM
#############

rosrun mine_detector svm_ex /hratc2015_workspace/src/orion/coil_signal_gen/config/trainingFile.txt