/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, UT Arlington
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of UT Arlington nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Isura Ranatunga
 *
 * FlexPlanner.h
 *  Created on: Jul 18, 2014
 */

#ifndef FLEXPLANNER_H_
#define FLEXPLANNER_H_

class FlexPlanner
{

  double m_xLen     ;
  double m_yLen     ;
  double robotWidth ;

  std::vector< Eigen::Vector3d,Eigen::aligned_allocator<Eigen::Vector3d> > m_path ;

public:

  FlexPlanner()
  {
    // TODO get from TF tree
    m_xLen = 10.0;
    m_yLen = 5.0;

    robotWidth = 1.0;
  }

  // FIXME check this
  bool XOR(bool a, bool b)
  {
      return ( (a && !b) || (!a && b) ) ;
  }

  void generatePath( double p_xLen, double p_yLen )
  {
    m_xLen = p_xLen  ;
    m_yLen = p_yLen  ;

    generatePath() ;
  }

  void generatePath()
  {
    m_path.push_back( Eigen::Vector3d( 0, 0, 0) )  ;
    double scale = m_xLen;
    if ( scale > m_yLen )
        scale = m_yLen;

    double scaledX = m_xLen / scale * robotWidth ;
    double scaledY = m_yLen / scale * robotWidth ;

    double dX = scaledX;
    double dY = scaledY;

    bool xXor = false;
    bool yXor = false;

    bool done = 0;

    while( !done )
    {
      double x = m_path.back()(0);
      double y = m_path.back()(1);

      m_path.push_back( Eigen::Vector3d( x+dX, y, 0) );

      dX = std::abs(dX) + scaledX;

      xXor = XOR(true,xXor);
      if( xXor )
          dX = -dX;

      x = m_path.back()(0);
      y = m_path.back()(1);

      if( std::abs(x) > m_xLen/2 || std::abs(y) > m_yLen/2 )
      {
        m_path.back()(0) = m_path.back()(0) - scaledX ;
        break;
      }

      x = m_path.back()(0);
      y = m_path.back()(1);

      m_path.push_back( Eigen::Vector3d( x, y+dY, 0) );

      dY = std::abs(dY) + scaledY;

      yXor = XOR(true,yXor);
      if( yXor )
          dY = -dY;

      x = m_path.back()(0);
      y = m_path.back()(1);

      if( std::abs(x) > m_xLen/2 || std::abs(y) > m_yLen/2 )
      {
        m_path.back()(1) = m_path.back()(1) - scaledY ;
        break;
      }
    }

    m_path.push_back( Eigen::Vector3d( 0, 0, 0) );

  }

  Eigen::Vector3d getPath( int i )
  {
    return m_path[ i ] ;
  }

  int getPathSize()
  {
    return m_path.size();
  }

  void initLengths( double p_xLen, double p_yLen )
  {
    m_xLen = p_xLen ;
    m_yLen = p_yLen ;
  }

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};

#endif /* FLEXPLANNER_H_ */
