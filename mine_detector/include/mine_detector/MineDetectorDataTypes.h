/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, UT Arlington
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of UT Arlington nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Isura Ranatunga
 *
 * MineDetectorDataTypes.h
 *  Created on: Jul 18, 2014
 */

#ifndef MINEDETECTORDATATYPES_H_
#define MINEDETECTORDATATYPES_H_

#include <metal_detector_msgs/Coil.h>
#include <geometry_msgs/PoseStamped.h>

#include <Eigen/Core>

struct SensorCoilStruct
{
  metal_detector_msgs::Coil   m_signal   ;
  geometry_msgs::PointStamped m_pointIn ;

  double m_detectionThresh ;

  std::vector< Eigen::Vector3d,Eigen::aligned_allocator<Eigen::Vector3d> > m_alertPoseArray  ;
  std::vector< Eigen::Vector4d,Eigen::aligned_allocator<Eigen::Vector4d> > m_poseArray       ;
  std::vector< Eigen::Vector4d,Eigen::aligned_allocator<Eigen::Vector4d> > m_estimationArray ;
};

struct MineDetectedStruct
{
  bool                       m_detected    ;
  geometry_msgs::PoseStamped m_location    ;
  double                     m_probability ;

  MineDetectedStruct()
  {
    m_detected = false ;
  }
};

#endif /* MINEDETECTORDATATYPES_H_ */
