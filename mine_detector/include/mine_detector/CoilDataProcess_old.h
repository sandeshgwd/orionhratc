/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, UT Arlington
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of UT Arlington nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Isura Ranatunga
 *
 * coilDataProcess.h
 *  Created on: May 6, 2014
 */

#ifndef COILDATAPROCESS_H_
#define COILDATAPROCESS_H_

#include <ros/ros.h>
#include <ros/package.h>
#include <string.h>

#include <metal_detector_msgs/Coil.h>
#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Geometry>

#include <vector>
#include <numeric>

#include <mine_detector/MineDetectorDataTypes.h>

#include <mine_detector/MineClassifier.h>

class CoilDataProcess
{
  ros::NodeHandle            m_node                  ;
  ros::Publisher             m_firstTimeDetectionPub ;
  ros::Publisher             m_nextCommandPub        ;

  MineDetectedStruct         m_mineDetected          ;
  geometry_msgs::PoseStamped m_minepose              ;

  MineClassifier::
  sample_type                m_sample                ;

//  MineClassifier*            m_classifier           ;
  MineClassifier*            m_classifierL            ;
  MineClassifier*            m_classifierM            ;
  MineClassifier*            m_classifierR            ;

  // Publish to Judge about mine
  std::vector< Eigen::Vector3d,Eigen::aligned_allocator<Eigen::Vector3d> > M_mineArray ;

  std::vector<double> m_detectionValue ;
  std::vector<double> m_classifierValue ;

  std::string         m_nameLeftCoil   ;
  std::string         m_nameMiddleCoil ;
  std::string         m_nameRightCoil  ;

  double              m_alpha              ;
  double			  m_checkMineRadius	   ;


public:

  SensorCoilStruct    m_leftCoil       ;
  SensorCoilStruct    m_middleCoil     ;
  SensorCoilStruct    m_rightCoil      ;

  int m_count_left;
  int m_count_middle;
  int m_count_right;
  int m_detect;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  CoilDataProcess()
  {
    m_leftCoil.m_signal.channel  .resize( 3 );
    m_middleCoil.m_signal.channel.resize( 3 );
    m_rightCoil.m_signal.channel .resize( 3 );

    m_leftCoil.m_signal.zero     .resize( 3 );
    m_middleCoil.m_signal.zero   .resize( 3 );
    m_rightCoil.m_signal.zero    .resize( 3 );

    /*m_leftCoil  .m_detectionThresh =  70000.0 ;
    m_middleCoil.m_detectionThresh =  70000.0 ;
    m_rightCoil .m_detectionThresh = 100000.0 ;*/

    //Real run threshold values
    m_leftCoil  .m_detectionThresh = 130000.0;//140000.0 ;
	m_middleCoil.m_detectionThresh =  90000.0; //100000.0;
	m_rightCoil .m_detectionThresh = 400000.0;//250000.0 ;

    m_nameLeftCoil   = "left_coil"   ;
    m_nameMiddleCoil = "middle_coil" ;
    m_nameRightCoil  = "right_coil"  ;

    m_alpha = 0                      ;
    m_count_left = 0					     ;
    m_count_middle = 0					     ;
    m_count_right = 0					     ;
    m_detect = 0					 ;
    m_checkMineRadius = 1.5			 ;

    m_firstTimeDetectionPub = m_node.advertise<geometry_msgs::PointStamped>( "/firstTimeMineDetected", 1 ) ;
    m_nextCommandPub = m_node.advertise<std_msgs::Empty>( "/nextCommand", 1 ) ;

    std::string path = ros::package::getPath("mine_detector");
//    m_classifier = new MineClassifier(path + "/config/simDetectorSVM.dat");
    m_classifierL = new MineClassifier(path + "/config/simDetectorSVMLeft.dat");
    m_classifierM = new MineClassifier(path + "/config/simDetectorSVMMiddle.dat");
    m_classifierR = new MineClassifier(path + "/config/simDetectorSVMRight.dat");
  }

  void setThreshold( double & p_leftCoilDetectionThresh, double & p_middleCoilDetectionThresh, double & p_rightCoilDetectionThresh )
  {
	  m_leftCoil  .m_detectionThresh = p_leftCoilDetectionThresh   ;
	  m_middleCoil.m_detectionThresh = p_middleCoilDetectionThresh ;
	  m_rightCoil .m_detectionThresh = p_rightCoilDetectionThresh  ;
  }

  double metalDetectionLevel( const SensorCoilStruct & coilData )
  {

    if( coilData.m_signal.zero[0] !=  coilData.m_signal.zero[1] )
      m_alpha = (-coilData.m_signal.zero[1] / ( coilData.m_signal.zero[0]-coilData.m_signal.zero[1] ) );
    //else (//Real Run Since zero data not published)
    //  m_alpha = 0 ;


    // Return detection alert
    return m_alpha*coilData.m_signal.channel[0] + (1 - m_alpha)*coilData.m_signal.channel[1];
  }

  double metalClassifierLevel( const SensorCoilStruct & coilData )
  {
    double beta = 0 ;

    if( coilData.m_signal.channel[0] != coilData.m_signal.zero[0] )
      beta = ( ( coilData.m_signal.channel[1] - coilData.m_signal.zero[1] ) / ( coilData.m_signal.channel[0] - coilData.m_signal.zero[0] ) );

    return beta ;
  }

  void saveCoilDataAndCheckMine( const geometry_msgs::PointStamped & point_out, const boost::shared_ptr<const metal_detector_msgs::Coil> & coil_ptr )
  {
    if( !strcmp( coil_ptr->header.frame_id.c_str(), m_nameLeftCoil.c_str()   ) )
    {
      m_leftCoil.m_signal   = *coil_ptr  ;
      m_leftCoil.m_pointIn =  point_out ;
      if (m_count_left == 0)
	  {
    	  m_leftCoil.m_signal.zero[0] = m_leftCoil.m_signal.channel[0];
    	  m_leftCoil.m_signal.zero[1] = m_leftCoil.m_signal.channel[1];
    	  m_leftCoil.m_signal.zero[2] = m_leftCoil.m_signal.channel[2];
    	  m_count_left++;
	  }

      checkForMine( m_leftCoil, m_mineDetected ) ;
      ROS_DEBUG_STREAM( m_nameLeftCoil );
    }

// this was commented because we were not using middle coil
    if( !strcmp( coil_ptr->header.frame_id.c_str(), m_nameMiddleCoil.c_str() ) )
    {
		m_middleCoil.m_signal   = *coil_ptr  ;
		m_middleCoil.m_pointIn =  point_out ;
		if (m_count_middle == 0)
		{
			m_middleCoil.m_signal.zero[0] = m_middleCoil.m_signal.channel[0];
			m_middleCoil.m_signal.zero[1] = m_middleCoil.m_signal.channel[1];
			m_middleCoil.m_signal.zero[2] = m_middleCoil.m_signal.channel[2];
			m_count_middle++;
		}
		checkForMine( m_middleCoil, m_mineDetected ) ;
		ROS_DEBUG_STREAM( m_nameMiddleCoil );
    }

    if( !strcmp( coil_ptr->header.frame_id.c_str(), m_nameRightCoil.c_str()  ) )
    {
    m_rightCoil.m_signal   = *coil_ptr  ;
    m_rightCoil.m_pointIn =  point_out ;
    if (m_count_right == 0)
	{
    	m_rightCoil.m_signal.zero[0] = m_rightCoil.m_signal.channel[0];
    	m_rightCoil.m_signal.zero[1] = m_rightCoil.m_signal.channel[1];
    	m_rightCoil.m_signal.zero[2] = m_rightCoil.m_signal.channel[2];
		m_count_right++;
	}
    checkForMine( m_rightCoil, m_mineDetected ) ;
    ROS_DEBUG_STREAM( m_nameRightCoil );
    }
  }



  void checkForMine( SensorCoilStruct & coilData, MineDetectedStruct & mineDetected )
  {
    double detection_alert = std::abs( metalDetectionLevel(  coilData ) ) ;
    double signature       = metalClassifierLevel( coilData ) ;
    double classifier = 0.0;

    ROS_DEBUG_STREAM( "Detection alert: " << detection_alert );
    ROS_DEBUG_STREAM( "Signature value: " << signature );

    m_sample(0) = coilData.m_signal.channel[0] - coilData.m_signal.zero[0] ;
	m_sample(1) = coilData.m_signal.channel[1] - coilData.m_signal.zero[1] ;
	m_sample(2) = coilData.m_signal.channel[2] - coilData.m_signal.zero[2] ;

	if(coilData.m_signal.header.frame_id.compare("left_coil") == 0)
	{
		classifier = m_classifierL->classify(m_sample);
		ROS_DEBUG_STREAM("the left classifier value:: "<< classifier);
	}

    else if(coilData.m_signal.header.frame_id.compare("middle_coil") == 0)
	{
		classifier = m_classifierM->classify(m_sample);
		ROS_DEBUG_STREAM("the midlle classifier value :: "<< classifier);
	}

    else if(coilData.m_signal.header.frame_id.compare("right_coil") == 0)
	{
		classifier = m_classifierR->classify(m_sample);
		ROS_DEBUG_STREAM("the right classifier value :: "<< classifier);
	}

    // LOGIC to Detect mines
    // e.g. detection_alert < -35000
    // Less than 0 for metals, more than 0 for mines
    if ( detection_alert > coilData.m_detectionThresh)
    {
      m_detect = 1;

      Eigen::Vector3d pose3d = msg2Eigen3d( coilData.m_pointIn ) ;

      if( coilData.m_poseArray.size() == 0 && !isInsideCircle( M_mineArray, pose3d, m_checkMineRadius ) )
      {
    	  geometry_msgs::PointStamped msg;
    	  msg.point.x = coilData.m_pointIn.point.x;
    	  msg.point.y = coilData.m_pointIn.point.y;

          m_firstTimeDetectionPub.publish( msg );
      }

      coilData.m_poseArray.push_back( Eigen::Vector4d( coilData.m_pointIn.point.x, coilData.m_pointIn.point.y, detection_alert,classifier ) );
      m_detectionValue.push_back( detection_alert ) ;
      m_classifierValue.push_back( classifier ) ;
    }

    if ( detection_alert < coilData.m_detectionThresh )
    {
    	/*if ( m_detect == 1 )
		{
    		m_count++;
			m_detect = 0;
		}*/

      if ( coilData.m_poseArray.size() > 0 )
      {
        double maxDetectionValue = *std::max_element(m_detectionValue.begin(), m_detectionValue.end());
        if (maxDetectionValue < 5.0*coilData.m_detectionThresh)
        {
			for ( int i = 0; i < coilData.m_poseArray.size(); i++ )
			{
				if ( coilData.m_poseArray[i](2) == maxDetectionValue )// && coilData.m_poseArray[i](3) > 0.5 )// Commented for real run(No training data)
				{
					coilData.m_alertPoseArray.push_back( Eigen::Vector3d( coilData.m_poseArray[i](0), coilData.m_poseArray[i](1), 0.0 ) );
				}
			}

			if( coilData.m_alertPoseArray.size() > 0 )
			{
			  Eigen::Vector3d sum = std::accumulate(coilData.m_alertPoseArray.begin(), coilData.m_alertPoseArray.end(), Eigen::Vector3d::Zero().eval());
			  Eigen::Vector3d m =  sum / coilData.m_alertPoseArray.size();

			  ROS_DEBUG_STREAM( m.x() << " " << m.y() << " " << m.z() << " " << coilData.m_alertPoseArray.size());

			  if( !isInsideCircle( M_mineArray, m, m_checkMineRadius ) ||  M_mineArray.size() == 0 )
			  {
				mineDetected.m_location.header.stamp = ros::Time::now() ;
				mineDetected.m_location.header.frame_id  = "minefield" ;
				mineDetected.m_detected = true ;
				mineDetected.m_location.pose.position.x = m.x() ;
				mineDetected.m_location.pose.position.y = m.y() ;
				mineDetected.m_location.pose.position.z = m.z() ;
				mineDetected.m_location.pose.orientation = tf::createQuaternionMsgFromYaw(0.0) ;

				M_mineArray.push_back( m ) ;
				ROS_WARN_STREAM( "Mine detected!" );
			  }

			}
        }

        std_msgs::Empty nextCommand;
		m_nextCommandPub.publish(nextCommand);

        coilData.m_alertPoseArray.clear();
        coilData.m_poseArray.clear();
        m_detectionValue.clear();

      }
    }
  }

  bool isInsideCircle( std::vector< Eigen::Vector3d,Eigen::aligned_allocator<Eigen::Vector3d> > & center, Eigen::Vector3d & point, double radius )
  {
    for( int i = 0; i < center.size(); i++ )
    {
    	ROS_DEBUG_STREAM( "Radius value::" << std::abs(( point - center[i] ).norm()));
      if( std::abs(( point - center[i] ).norm())  < radius )
        return true;
    }

    return false;
  }

  Eigen::Vector3d msg2Eigen3d( const geometry_msgs::PointStamped & pose )
  {
    return Eigen::Vector3d( pose.point.x, pose.point.y, pose.point.z );
  }


  void readCoil(const geometry_msgs::PointStamped & point_out, const boost::shared_ptr<const metal_detector_msgs::Coil> & coil_ptr, MineDetectedStruct & mineDetected )
  {
    saveCoilDataAndCheckMine( point_out, coil_ptr ) ;
    mineDetected = m_mineDetected ;
    m_mineDetected.m_detected = false ;
  }

  SensorCoilStruct getLeftCoil()
  {
    return m_leftCoil;
  }

  SensorCoilStruct getMiddleCoil()
  {
    return m_middleCoil;
  }

  SensorCoilStruct getRightCoil()
  {
    return m_rightCoil;
  }

};


#endif /* COILDATAPROCESS_H_ */
