#include <iostream>
#include <dlib/svm.h>

#include <fstream>      // fstream
#include <vector>
#include <string>

using namespace std;
using namespace dlib;
using namespace boost;

class MineClassifier
{
public:

	typedef matrix<double, 1, 1> sample_type;

	MineClassifier( string filename )
	{
		// Load the function object
    	ifstream fin(filename.c_str(),ios::binary);
    	deserialize(learned_function, fin);
    	fin.close();
	}

	double classify( sample_type sample )
	{
		return learned_function(sample);
	}

private:
	typedef radial_basis_kernel<sample_type> kernel_type;

    typedef decision_function<kernel_type> dec_funct_type;
    typedef normalized_function<dec_funct_type> funct_type;

    std::vector<sample_type> samples;

    funct_type learned_function;
};


