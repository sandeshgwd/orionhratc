/*
 * Sandesh Gowda
 * 03/10/2015
 * Test run to estimate the center of the Gaussian Field
 *
 * Matlab code to get the Gaussian plot
resolution = 0.01;
column = 0:resolution:10; % x - axis
row = 0:resolution:10; % y - axis
[X, Y] = meshgrid(column, row);
amplitude = 1;
sigma_x = 0.5;
sigma_y = 0.5;
a = 1/(2*sigma_x^2) ;
b = 1/(2*sigma_y^2) ;
mines   = [5,2.5]';
mineMap = 0;
mineMap = mineMap + amplitude*exp( - ((a*(X-mines(1,i)).^2) + (b*(Y-mines(2,i)).^2))) ;
surf(mineMap)
 */

#ifndef CENTERESTIMATON_H_
#define CENTERESTIMATON_H_

#include <ros/ros.h>
#include <math.h>
#include <nlopt.h>
#include <iostream>
#include <fstream>
#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Geometry>
#include <vector>
#include <numeric>

typedef struct {
    double a, b, c;
} my_constraint_data;

typedef std::vector < Eigen::Vector4d,Eigen::aligned_allocator<Eigen::Vector4d> > eigen4Darray;


//contrains in the form of 0>=f(x,y)
double myconstraint(unsigned n, const double *x, double *grad, void *data)
{
	my_constraint_data *d = (my_constraint_data *) data ;
	double a = d->a, b = d->b, c = d->c				    ;
	if (grad) {
		grad[0] = b            ;
		grad[1] = c            ;
	}
	return (a + b*x[0] + c*x[1]) ;
}
double myfunc(unsigned n, const double *x, double *grad, void *my_func_data)
{
	eigen4Darray* mData = (eigen4Darray*) my_func_data ;
	double minValue = 0.0                              ;
	double sigma = 0.14                                ;
	double a = 1/( 2 *( pow(sigma,2) ) )               ;
	double leftAmplitude = 96250                       ;
	double middleAmplitude = 96250                     ;
	double rightAmplitude = 96250                      ;
	double amplitude								   ;
	if(mData->at(0)(3) == 0)
	{
		amplitude = leftAmplitude;
	}
	else if(mData->at(0)(3) == 1)
	{
		amplitude = middleAmplitude;
	}
	else if(mData->at(0)(3) == 2)
	{
		amplitude = rightAmplitude;
	}

	for (int i = 0; i < 9;i++)
	{
		minValue = minValue + fabs( mData->at(i)(2) - (amplitude*exp( -a * ( pow(mData->at(i)(0)-x[0],2) + pow(mData->at(i)(1)-x[1],2)) ) ) );
	}
	return minValue;
}
Eigen::Vector2d centerEstimation(eigen4Darray mineData)
{
	double lb[2] = { -10000.0, 0 }              ; /* lower bounds */
	nlopt_opt opt                               ;
	opt = nlopt_create(NLOPT_LN_BOBYQA, 2)      ; /* algorithm and dimensionality */
	nlopt_set_lower_bounds(opt, lb)             ;
	nlopt_set_min_objective(opt, myfunc, &mineData)  ;
	Eigen::Vector4d add = std::accumulate(mineData.begin(), mineData.end(), Eigen::Vector4d::Zero().eval());
	Eigen::Vector4d avg =  add / mineData.size();

//	ROS_INFO_STREAM("the avg is ::"<< avg.w() << " " << avg.x() << " " << avg.y() << " "<< avg.z() );

	double xmin = avg.x() - 0.5;
	double xmax = avg.x() + 0.5;
	double ymin = avg.y() - 0.5;
	double ymax = avg.y() + 0.5;
	my_constraint_data data[4] = { {xmin,-1,0}, {-xmax,1,0}, {ymin,0,-1}, {-ymax,0,1} };
	nlopt_add_inequality_constraint(opt, myconstraint, &data[0], 1e-8)          ;
	nlopt_add_inequality_constraint(opt, myconstraint, &data[1], 1e-8)          ;
	nlopt_add_inequality_constraint(opt, myconstraint, &data[2], 1e-8)          ;
	nlopt_add_inequality_constraint(opt, myconstraint, &data[3], 1e-8)          ;

	nlopt_set_xtol_rel(opt, 1e-8) ;
	double x[2] = { avg.x(), avg.y() };
	double minf; /* the minimum objective value, upon return */

	if (nlopt_optimize(opt, x, &minf) < 0)
	{
		ROS_INFO("nlopt failed!");
	}
	else
	{
		return Eigen::Vector2d(x[0],x[1]);
	}
}
#endif /* CENTERESTIMATON_H_ */
