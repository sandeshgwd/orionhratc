/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, UT Arlington
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of UT Arlington nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Isura Ranatunga
 *
 * mineDetectorNode.cpp
 *  Created on: Apr 14, 2014
 */

#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <tf/tf.h>
#include <trajectory_msgs/JointTrajectory.h>

#include <control_msgs/FollowJointTrajectoryAction.h>

#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include <metal_detector_msgs/Coil.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/OccupancyGrid.h>

#include <Eigen/Core>
#include <vector>
#include <string>
#include <algorithm>

#include <mine_detector/FlexPlanner.h>
#include <mine_detector/MineDetectorDataTypes.h>

class RobotCommander
{
public:
    RobotCommander( ros::NodeHandle &node );
    void robotCommander();

private:

    void moveBaseCommand();

    void commandDoneCallback( const actionlib::SimpleClientGoalState       &state ,
                                 const move_base_msgs::MoveBaseResultConstPtr &result  ) ;
    void commandActiveCallback();
    void commandFeedbackCallback( const move_base_msgs::MoveBaseFeedbackConstPtr &feedback );

    void backupDoneCallback( const actionlib::SimpleClientGoalState       &state ,
                                const move_base_msgs::MoveBaseResultConstPtr &result  ) ;
    void backupActiveCallback();
    void backupFeedbackCallback( const move_base_msgs::MoveBaseFeedbackConstPtr &feedback );

    void setMineCallback(const geometry_msgs::PoseStampedPtr& msg);

    void getGlobalCostMapCallback( const nav_msgs::OccupancyGridPtr& msg );

    void firstTimeDetectionCallback( const geometry_msgs::PointStamped & msg ) ;

    void flexPlannercallBack( const geometry_msgs::Point & dimension ) ;

    void nextCommand( const std_msgs::Empty & msg ) ;

    actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> m_moveBaseClient;

    ros::Publisher m_cmd_vel_pub;	//for publishing the velocity commands

    ros::Subscriber m_setMineSub            ;
//    ros::Subscriber m_getGlobalCostMapSub   ;
    ros::Subscriber m_firstTimeDetectionSub ;
    ros::Subscriber m_nextCommandSub        ;
    ros::Subscriber m_minefieldDimension_sub;

    ros::Time m_startTime ;

    geometry_msgs::PoseWithCovarianceStamped m_odometry ;

    double m_x            ;
    double m_y            ;
    double m_yaw          ;

    int    m_randomRadius ;
    int    m_failedFlag   ;
    int    m_nodeIndex    ;

    int m_count			  ;

    move_base_msgs::MoveBaseGoal m_goalCommand    ;

    FlexPlanner                  m_flexPlannerObj ;

    tf::TransformListener        m_listener       ;

};

RobotCommander::RobotCommander( ros::NodeHandle &node ) : m_moveBaseClient("move_base", true)
{
  double m_widthMeters     ;
  double m_heightMeters    ;
  double m_imageResolution ;

  node.param( "global_costmap/width"     , m_widthMeters    , 100.0 ) ;
  node.param( "global_costmap/height"    , m_heightMeters   , 100.0 ) ;
  node.param( "global_costmap/resolution", m_imageResolution, 0.05  ) ;

  m_cmd_vel_pub = node.advertise<geometry_msgs::Twist>("/husky/cmd_vel", 100);

  m_nextCommandSub        = node.subscribe("/nextCommand", 10, &RobotCommander::nextCommand, this);

  m_setMineSub            = node.subscribe("/HRATC_FW/set_mine", 1, &RobotCommander::setMineCallback, this);
//  m_getGlobalCostMapSub   = node.subscribe("/move_base/global_costmap/costmap", 1, &RobotCommander::getGlobalCostMapCallback, this);
  m_firstTimeDetectionSub = node.subscribe( "/firstTimeMineDetected", 1, &RobotCommander::firstTimeDetectionCallback, this) ;

  // Wait for base server

  m_minefieldDimension_sub = node.subscribe("/HRATC_FW/field_dimension", 1000,&RobotCommander::flexPlannercallBack,this);

  m_x         = 0 ;
  m_y         = 0 ;
  m_yaw       = 0 ;

  m_nodeIndex    = 0 ;
  m_randomRadius = 0 ;
  m_failedFlag   = 0 ;

  m_count = 0;

}

void RobotCommander::robotCommander()
{

  m_randomRadius = 1 ;
  m_failedFlag   = 0 ;

  m_goalCommand.target_pose.header.frame_id = "/minefield"     ;
  m_goalCommand.target_pose.header.stamp    = ros::Time::now() ;

  if( m_nodeIndex < m_flexPlannerObj.getPathSize() )
  {
    // Insert your code here
    m_goalCommand.target_pose.pose.position.x  = m_flexPlannerObj.getPath( m_nodeIndex )( 0 ) ;
    m_goalCommand.target_pose.pose.position.y  = m_flexPlannerObj.getPath( m_nodeIndex )( 1 ) ;
    m_yaw                                      = m_flexPlannerObj.getPath( m_nodeIndex )( 2 ) ;

    m_goalCommand.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(m_yaw)     ;

    ROS_INFO("------------ robotCommander ------------");
    ROS_INFO("------------ GOAL POINT IS  = %lf %lf", m_goalCommand.target_pose.pose.position.x,
                                                      m_goalCommand.target_pose.pose.position.y  );

    m_startTime = ros::Time::now();

    m_moveBaseClient.sendGoal( m_goalCommand, boost::bind(&RobotCommander::commandDoneCallback    , this, _1, _2 ),
                                              boost::bind(&RobotCommander::commandActiveCallback  , this         ),
                                              boost::bind(&RobotCommander::commandFeedbackCallback, this, _1     ) );
  }else
  {
	ROS_INFO("------------ PATH COMPLETE ------------");
  }

}

void RobotCommander::moveBaseCommand()
{
  m_moveBaseClient.sendGoal( m_goalCommand, boost::bind(&RobotCommander::commandDoneCallback    , this, _1, _2 ),
                                            boost::bind(&RobotCommander::commandActiveCallback  , this         ),
                                            boost::bind(&RobotCommander::commandFeedbackCallback, this, _1     ) );

  ROS_INFO("------------ GOAL POINT IS  = %lf %lf", m_goalCommand.target_pose.pose.position.x,
                                                    m_goalCommand.target_pose.pose.position.y  );
}

void RobotCommander::commandDoneCallback( const actionlib::SimpleClientGoalState       &state ,
                                          const move_base_msgs::MoveBaseResultConstPtr &result  )
{
    if(state.state_ == actionlib::SimpleClientGoalState::SUCCEEDED )
    {
        m_nodeIndex++;

        ROS_INFO("The m_goalCommand was reached!");
        m_randomRadius = 2.0 ;
        m_failedFlag   = 0   ;

        robotCommander();
    }

    if(state.state_ == actionlib::SimpleClientGoalState::ABORTED )
    {
        ROS_WARN("Failed to reach the m_goalCommand...");

        ROS_INFO("Resetting goal point!");

        ROS_INFO_STREAM("Radius: " << m_randomRadius );

        // v3 = rand() % 30 + 1985; from 1985 to 2014
//        goalPoints[i][0] = goalPoints[i][0] + ( (double) ( rand() % m_randomRadius ) - m_randomRadius /2.0 );
//        goalPoints[i][1] = goalPoints[i][1] + ( (double) ( rand() % m_randomRadius ) - m_randomRadius /2.0 );

        // TODO
        // TODO
        // TODO add condition to stay inside arena
//        m_flexPlannerObj.getPath( m_nodeIndex )( 0 ) = m_flexPlannerObj.getPath( m_nodeIndex )( 0 ) +  ( (double) ( rand() % m_randomRadius ) - m_randomRadius /2.0 ) ;
//        m_flexPlannerObj.getPath( m_nodeIndex )( 1 ) = m_flexPlannerObj.getPath( m_nodeIndex )( 1 ) +  ( (double) ( rand() % m_randomRadius ) - m_randomRadius /2.0 ) ;

        m_flexPlannerObj.getPath( m_nodeIndex )( 0 ) = (m_flexPlannerObj.getPath( m_nodeIndex )( 0 ) +  m_flexPlannerObj.getPath( m_nodeIndex + 1)( 0 ) )/2;
		m_flexPlannerObj.getPath( m_nodeIndex )( 1 ) = (m_flexPlannerObj.getPath( m_nodeIndex )( 1 ) +  m_flexPlannerObj.getPath( m_nodeIndex + 1)( 1 ) )/2;

//        goalPoints[i][2] ;

        if( m_failedFlag <= 3 )
        {
          m_randomRadius = m_randomRadius + 1 ;
          m_failedFlag = 0 ;
        }

        m_failedFlag++;

        robotCommander();
    }


}

void RobotCommander::commandActiveCallback()
{
    ROS_INFO("The new command is active!");
}

void RobotCommander::commandFeedbackCallback( const move_base_msgs::MoveBaseFeedbackConstPtr &feedback )
{
    ROS_DEBUG("Getting feedback!");
/*
    m_count++;
	if(m_count >200)
	{
		m_moveBaseClient.cancelAllGoals();
		moveBaseCommand();
		m_count = 0;
	}
*/
}

void RobotCommander::backupDoneCallback( const actionlib::SimpleClientGoalState       &state ,
                                                  const move_base_msgs::MoveBaseResultConstPtr &result  )
{
    if(state.state_ == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        ROS_INFO("Successfully backed up!");

        moveBaseCommand();
    }

    if(state.state_ == actionlib::SimpleClientGoalState::ABORTED)
    {
        ROS_WARN("Failed to back up...");
    }
}

void RobotCommander::backupActiveCallback()
{
    ROS_INFO("Backing up!");
}

void RobotCommander::backupFeedbackCallback( const move_base_msgs::MoveBaseFeedbackConstPtr &feedback )
{
    ROS_DEBUG("Backing up!");
}

void RobotCommander::getGlobalCostMapCallback( const nav_msgs::OccupancyGridPtr& msg )
{
  msg->data ;
}

void RobotCommander::setMineCallback( const geometry_msgs::PoseStampedPtr& msg )
{
  ROS_INFO("Mine detected! Resetting command!");

  m_moveBaseClient.cancelAllGoals();

  geometry_msgs::Twist abc ;
  abc.linear.x = -1.5;
  abc.angular.z = 0.0;
  m_cmd_vel_pub.publish(abc);
  ros::Duration(5).sleep();

//  // Move back
//  move_base_msgs::MoveBaseGoal backupCommand ;
//  backupCommand.target_pose.header.frame_id = "/minefield";
//  backupCommand.target_pose.header.stamp = ros::Time::now();
//  backupCommand.target_pose.pose.position.x = msg->pose.position.x - 0.5 ;
//  backupCommand.target_pose.pose.position.y = msg->pose.position.y - 0.5 ;
//  backupCommand.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw( 1.5 );
//
//  m_moveBaseClient.sendGoal( backupCommand, boost::bind(&OrionMineDetector::backupDoneCallback    , this, _1, _2 ),
//                                            boost::bind(&OrionMineDetector::backupActiveCallback  , this         ),
//                                            boost::bind(&OrionMineDetector::backupFeedbackCallback, this, _1     ) );

	robotCommander();
}

void RobotCommander::nextCommand( const std_msgs::Empty & msg )
{
  ROS_INFO("Detection done! Starting new!");
  robotCommander();
}

void RobotCommander::firstTimeDetectionCallback( const geometry_msgs::PointStamped & msg )
{
  ROS_INFO("First time detection! Pausing!");

  m_moveBaseClient.cancelAllGoals();

  // Move back
/*  move_base_msgs::MoveBaseGoal backupCommand ;
  backupCommand.target_pose.header.frame_id = "/minefield";
  backupCommand.target_pose.header.stamp = ros::Time::now();
  m_moveBaseClient.sendGoal( backupCommand ) ;*/


  /*m_goalCommand.target_pose.pose.position.x  = msg.point.x ;
  m_goalCommand.target_pose.pose.position.y  = msg.point.y ;
  m_goalCommand.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(0.0)     ;
  moveBaseCommand();*/

//  ros::Duration(20).sleep();
//
//  robotCommander();
}

void RobotCommander::flexPlannercallBack(const geometry_msgs::Point & dimension)
{
	if(m_count < 1)
	{
		m_flexPlannerObj.generatePath( dimension.x, dimension.y ) ;

		ROS_INFO("Waiting for the move_base action server to come online...") ;
		if(!m_moveBaseClient.waitForServer(ros::Duration(50.0)))
		{
			ROS_FATAL("Did you forget to launch the ROS navigation?") ;
			ROS_BREAK();
		}
		m_count++;
		robotCommander();
	}
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "Team_Orion_Robot_Commander");

    ROS_INFO("Starting Mine Detection!");

    ros::NodeHandle node("~");

    RobotCommander orionMineDetectorObject( node );
//    orionMineDetectorObject.m_flexPlannerObj.generatePath(10.0,5.0);
//    orionMineDetectorObject.robotCommander();
    ros::spin();
    return 0;
}
