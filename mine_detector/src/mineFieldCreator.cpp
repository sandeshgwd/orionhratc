/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2015, UT Arlington
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of UT Arlington nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Sandesh Gowda
 *
 * mineFieldCreator.cpp
 *  Created on: May 4, 2015
 */

#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <tf/tf.h>
#include <trajectory_msgs/JointTrajectory.h>

#include <control_msgs/FollowJointTrajectoryAction.h>

#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include <metal_detector_msgs/Coil.h>

#include <geometry_msgs/Point.h>
#include <geometry_msgs/PoseStamped.h>

#include <Eigen/Core>
#include <vector>
#include <string>
#include <algorithm>

#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>

class mineFieldCreator
{
public:
	mineFieldCreator(ros::NodeHandle &node);
	ros::Publisher m_minefieldDimension_pub  ;
	//ros::Subscriber m_minefieldCorners_sub   ;
	void MineFieldCreator(geometry_msgs::Point dimension)
	{
		m_minefieldDimension_pub.publish(dimension);
	}
	geometry_msgs::Point fieldDimension;
	int m_count;
	void fieldCornerFunct();

private:
	ros::NodeHandle node;
	std::vector< Eigen::Vector3d,Eigen::aligned_allocator<Eigen::Vector3d> > m_corners ;
	visualization_msgs::Marker m_cornerMarkers ;
	//void fieldCornersCb(const boost::shared_ptr<visualization_msgs::MarkerArray> & fieldcorners);

	std::string centerFrame ;

	double xLen;
	double yLen;
	tf::TransformListener        m_listener       ;

};

void mineFieldCreator::fieldCornerFunct()
{
	visualization_msgs::MarkerArrayConstPtr fieldcorners = ros::topic::waitForMessage<visualization_msgs::MarkerArray>("/corners");
	try
	{
		m_listener.waitForTransform(centerFrame, fieldcorners->markers[0].header.frame_id, ros::Time::now(), ros::Duration(5.0));
	}
	catch (tf::TransformException ex)
	{
		ROS_ERROR("%s",ex.what()) ;
	}
	for ( int i = 0; i < fieldcorners->markers.size(); i++ )
	{
		m_cornerMarkers = fieldcorners->markers[i];

		geometry_msgs::PointStamped cornerPos ;

		cornerPos.header.frame_id = m_cornerMarkers.header.frame_id ;
		cornerPos.header.stamp = ros::Time()  ;

		cornerPos.point.x = m_cornerMarkers.pose.position.x ;
		cornerPos.point.y = m_cornerMarkers.pose.position.y ;
		cornerPos.point.z = m_cornerMarkers.pose.position.z ;
		ROS_INFO("cornerPos = %g %g %g",cornerPos.point.x,cornerPos.point.y,cornerPos.point.z) ;

		geometry_msgs::PointStamped	 corner_in_minefield ;

		try
		{
			m_listener.transformPoint( centerFrame, cornerPos, corner_in_minefield);
//				m_corners.push_back((double)m_cornerMarkers.id,(double)corner_in_minefield.point.x,(double)corner_in_minefield.point.y);
		}
		catch (tf::TransformException ex)
		{
			ROS_ERROR("%s",ex.what()) ;
		}

		m_corners.push_back(Eigen::Vector3d((double)m_cornerMarkers.id, corner_in_minefield.point.x, corner_in_minefield.point.y) )                   ;
		ROS_INFO("corner = %g %g %g",m_corners[i](0),m_corners[i](1),m_corners[i](2))                    ;
	}

	double len1 =  std::max( sqrt( pow(m_corners[0](1)-m_corners[1](1), 2) + pow(m_corners[0](2)-m_corners[1](2), 2) ), sqrt( pow(m_corners[2](1)-m_corners[3](1), 2) + pow(m_corners[2](2)-m_corners[3](2), 2) ) ) ;
	double len2 =  std::max( sqrt( pow(m_corners[0](1)-m_corners[3](1), 2) + pow(m_corners[0](2)-m_corners[3](2), 2) ), sqrt( pow(m_corners[2](1)-m_corners[1](1), 2) + pow(m_corners[2](2)-m_corners[1](2), 2) ) ) ;
	xLen = std::max(len1,len2);
	yLen = std::min(len1,len2);
	fieldDimension.x = xLen;
	fieldDimension.y = yLen;
	fieldDimension.z = 0.0;
}
/*void mineFieldCreator::fieldCornersCb(const boost::shared_ptr<visualization_msgs::MarkerArray> & fieldcorners)
{//minefield corners subscribing through rostopic corners
	try
	{
		m_listener.waitForTransform(centerFrame, fieldcorners->markers[0].header.frame_id, ros::Time::now(), ros::Duration(1.0));
	}
	catch (tf::TransformException ex)
	{
		ROS_ERROR("%s",ex.what()) ;
	}
	if(m_count < 1)
	{

		for ( int i = 0; i < fieldcorners->markers.size(); i++ )
		{
			m_count++;
			m_cornerMarkers = fieldcorners->markers[i];

			geometry_msgs::PointStamped cornerPos ;

			cornerPos.header.frame_id = m_cornerMarkers.header.frame_id ;
			cornerPos.header.stamp = ros::Time()  ;

			cornerPos.point.x = m_cornerMarkers.pose.position.x ;
			cornerPos.point.y = m_cornerMarkers.pose.position.y ;
			cornerPos.point.z = m_cornerMarkers.pose.position.z ;
//			ROS_INFO("cornerPos = %g %g %g",cornerPos.point.x,cornerPos.point.y,cornerPos.point.z)                    ;
			geometry_msgs::PointStamped	 corner_in_minefield ;

			try
			{
				m_listener.waitForTransform(centerFrame, cornerPos.header.frame_id, ros::Time::now(), ros::Duration(1.0));
				m_listener.transformPoint( centerFrame, cornerPos, corner_in_minefield);
//				m_corners.push_back((double)m_cornerMarkers.id,(double)corner_in_minefield.point.x,(double)corner_in_minefield.point.y);
			}
			catch (tf::TransformException ex)
			{
				ROS_ERROR("%s",ex.what()) ;
			}

			m_corners.push_back(Eigen::Vector3d((double)m_cornerMarkers.id, corner_in_minefield.point.x, corner_in_minefield.point.y) )                   ;
			ROS_INFO("corner = %g %g %g",m_corners[i](0),m_corners[i](1),m_corners[i](2))                    ;
		}

		double len1 =  std::max( sqrt( pow(m_corners[0](1)-m_corners[1](1), 2) + pow(m_corners[0](2)-m_corners[1](2), 2) ), sqrt( pow(m_corners[2](1)-m_corners[3](1), 2) + pow(m_corners[2](2)-m_corners[3](2), 2) ) ) ;
		double len2 =  std::max( sqrt( pow(m_corners[0](1)-m_corners[3](1), 2) + pow(m_corners[0](2)-m_corners[3](2), 2) ), sqrt( pow(m_corners[2](1)-m_corners[1](1), 2) + pow(m_corners[2](2)-m_corners[1](2), 2) ) ) ;
		xLen = std::max(len1,len2);
		yLen = std::min(len1,len2);
		fieldDimension.x = xLen;
		fieldDimension.y = yLen;
		fieldDimension.z = 0.0;
		MineFieldCreator(fieldDimension);
	}

}*/

mineFieldCreator::mineFieldCreator(ros::NodeHandle &node)
{
	m_minefieldDimension_pub = node.advertise<geometry_msgs::Point>("/HRATC_FW/field_dimension", 1000);
//	m_minefieldCorners_sub = node.subscribe("/corners", 1000, &mineFieldCreator::fieldCornersCb, this);
	centerFrame = "minefield";
	node.param( "xLen" , xLen, 12.0 )             ;
	node.param( "yLen" , yLen,  7.0 )             ;
	m_count = 0;
	fieldDimension.x = xLen;
	fieldDimension.y = yLen;
	fieldDimension.z = 0.0;
//	MineFieldCreator(fieldDimension);
//	ros::spinOnce();
}
/*
mineFieldCreator::mineFieldCreator(ros::NodeHandle &node)
{
	m_minefieldDimension_pub = node.advertise<geometry_msgs::Point>("/HRATC_FW/field_dimension", 1000);
	m_minefieldCorners_sub = node.subscribe("/corners", 1000, &mineFieldCreator::mineDetectedCb, this);

	centerFrame = "/minefield_corners" ;

	node.param( "xLen" , xLen, 12.0 ) ;
	node.param( "yLen" , yLen,  7.0 ) ;
	geometry_msgs::PointStamped cornerPos ;

	//just an arbitrary point in space
	cornerPos.point.x = 0.0 ;
	cornerPos.point.y = 0.0 ;
	cornerPos.point.z = 0.0 ;

	geometry_msgs::PointStamped corner_in_minefield ;

	cornerPos.header.frame_id = "corner1" ;
	cornerPos.header.stamp = ros::Time()  ;
	try
	{
		m_listener.transformPoint(centerFrame, cornerPos, corner_in_minefield) ;
		corner1 = geomMsgToTfVec( corner_in_minefield ) ;
	}
	catch (tf::TransformException ex)
	{
		ROS_ERROR("%s",ex.what()) ;
	}

	cornerPos.header.frame_id = "corner2" ;
	cornerPos.header.stamp = ros::Time()  ;
	try
	{
		m_listener.transformPoint(centerFrame, cornerPos, corner_in_minefield) ;
		corner2 = geomMsgToTfVec( corner_in_minefield ) ;
	}
	catch (tf::TransformException ex)
	{
		ROS_ERROR("%s",ex.what()) ;
	}

	cornerPos.header.frame_id = "corner3" ;
	cornerPos.header.stamp = ros::Time()  ;
	try
	{
		m_listener.transformPoint(centerFrame, cornerPos, corner_in_minefield) ;
		corner3 = geomMsgToTfVec( corner_in_minefield ) ;
	}
	catch (tf::TransformException ex)
	{
		ROS_ERROR("%s",ex.what()) ;
	}

	cornerPos.header.frame_id = "corner4" ;
	cornerPos.header.stamp = ros::Time()  ;
	try
	{
		m_listener.transformPoint(centerFrame, cornerPos, corner_in_minefield ) ;
		corner4 = geomMsgToTfVec( corner_in_minefield ) ;

		// Only do this if corner exists
		xLen =  std::max( sqrt(pow(corner4.x()-corner1.x(), 2) + pow(corner4.y()-corner1.y(), 2) + pow(corner4.z()-corner1.z(), 2)), sqrt(pow(corner3.x()-corner2.x(), 2) + pow(corner3.y()-corner2.y(), 2) + pow(corner3.z()-corner2.z(), 2)) ) ;
		yLen =  std::max( sqrt(pow(corner2.x()-corner1.x(), 2) + pow(corner2.y()-corner1.y(), 2) + pow(corner2.z()-corner1.z(), 2)), sqrt(pow(corner4.x()-corner3.x(), 2) + pow(corner4.y()-corner3.y(), 2) + pow(corner4.z()-corner3.z(), 2)) ) ;
	}
	catch (tf::TransformException ex)
	{
		ROS_ERROR("%s",ex.what()) ;
	}

	fieldDimension.x = xLen;
	fieldDimension.y = yLen;
	fieldDimension.z = 0.0;
}
*/

int main(int argc, char **argv)
{
	ros::init(argc, argv, "Mine_Field_Creator");

	ros::NodeHandle n;
	mineFieldCreator MineField(n);
	ros::Rate r(50);
	MineField.fieldCornerFunct();
	while ( n.ok())
	{
		MineField.MineFieldCreator(MineField.fieldDimension);
		ros::spinOnce();
		r.sleep();
	}
//	ros::spin();

	return 0;
}

