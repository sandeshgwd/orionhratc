/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, UT Arlington
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of UT Arlington nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Isura Ranatunga
 *
 * mineMap.cpp
 *  Created on: May 7, 2014
 */

#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/GetMap.h>

#include <Eigen/Core>

#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include <metal_detector_msgs/Coil.h>


class ScanMap
{
public:
  ScanMap() : m_targetFrame("minefield") , m_tf()
    {
      m_mdSub.subscribe(m_node, "coils", 1000);
      m_tfFilter = new tf::MessageFilter<metal_detector_msgs::Coil>(m_mdSub, m_tf, m_targetFrame, 1000);
      m_tfFilter->registerCallback( boost::bind(&ScanMap::msgCallback, this, _1) );

      m_scanMapPub      = m_node.advertise<nav_msgs::OccupancyGrid> ("scan_map", 1000, false);
      m_scanMapMetaPub  = m_node.advertise<nav_msgs::MapMetaData>("scan_map_metadata", 1000, true);

      m_height          = 2000 ;
      m_width           = 2000 ;
      m_resolution      = 0.05 ; // m/pixel
      m_fieldLength     = 100  ;
      m_fieldBredth     = 100  ;

      m_mapStartPose.position.x = - m_fieldLength/2.0 ;
      m_mapStartPose.position.y = - m_fieldBredth/2.0 ;

      m_scanMapImage    = cv::Mat::zeros(m_height, m_width, CV_8UC1);
    }

private:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    std::string        m_targetFrame     ;

    ros::NodeHandle    m_node            ;
    ros::Publisher     m_scanMapPub      ;
    ros::Publisher     m_scanMapMetaPub  ;

    message_filters::Subscriber<metal_detector_msgs::Coil> m_mdSub;
    tf::TransformListener m_tf;
    tf::MessageFilter<metal_detector_msgs::Coil> * m_tfFilter;

    nav_msgs::OccupancyGrid m_scanMap    ;
    cv::Mat m_scanMapImage               ;

    int    m_height                      ;
    int    m_width                       ;
    double m_resolution                  ;
    double m_fieldLength                 ;
    double m_fieldBredth                 ;

    geometry_msgs::Pose m_mapStartPose   ;

public:

    void setInitMap( )
    {
      ROS_INFO("Initilized Mine Map!");

      m_scanMap                    = globalImageToNavMsg() ;
      m_scanMap.header.frame_id    = m_targetFrame         ;
      m_scanMap.header.stamp       = ros::Time::now()     ;
      m_scanMap.info.map_load_time = ros::Time::now()     ;
      m_scanMap.info.origin        = m_mapStartPose        ;
    }

    //  Callback to register with tf::MessageFilter to be called when transforms are available
    void msgCallback(const boost::shared_ptr<const metal_detector_msgs::Coil>& coilPtr)
    {
        geometry_msgs::PointStamped   pointIn                  ;
        pointIn.header.frame_id     = coilPtr->header.frame_id ;
        pointIn.header.stamp        = coilPtr->header.stamp    ;
        pointIn.point.x             = 0.0                      ;
        pointIn.point.y             = 0.0                      ;
        pointIn.point.z             = 0.0                      ;

        geometry_msgs::PointStamped pointout;

        try
        {
          m_tf.transformPoint( m_targetFrame, pointIn, pointout ) ;

          // Set mine
          m_scanMapImage.at<uchar>( cv::Point((int) ( pointout.point.x/m_resolution + m_height/2.0 ), (int) ( pointout.point.y/m_resolution + m_width/2.0 ) ) ) = 100 ;


          ROS_DEBUG("Updated Scan Map!");

          m_scanMap                    = globalImageToNavMsg() ;
          m_scanMap.header.frame_id    = m_targetFrame         ;
          m_scanMap.header.stamp       = ros::Time::now()      ;
          m_scanMap.info.map_load_time = ros::Time::now()      ;
          m_scanMap.info.origin        = m_mapStartPose        ;

          m_scanMapPub.publish( m_scanMap );
          m_scanMapMetaPub.publish( m_scanMap.info );

         }catch (tf::TransformException &ex)
         {
             ROS_WARN("Failure %s\n", ex.what());
         }

    }

    nav_msgs::OccupancyGrid globalImageToNavMsg()
    {
      nav_msgs::OccupancyGrid localScanMap               ;

      localScanMap.info.resolution = m_resolution        ;
      localScanMap.info.width      = m_scanMapImage.cols ;
      localScanMap.info.height     = m_scanMapImage.rows ;

      // TODO Better way to do this
      // TODO rows/cols may be flipped
      for (unsigned i=0; i < localScanMap.info.height; ++i)
      {
        for (unsigned j=0; j < localScanMap.info.width; ++j)
        {
          // TODO check this
            localScanMap.data.push_back( m_scanMapImage.at<uchar>(i,j) );
        }
      }

      return localScanMap ;
    }

    void updateMinemap()
    {
      ros::Rate r(5);
      while ( m_node.ok())
      {
        m_scanMapPub.publish( m_scanMap );
        m_scanMapMetaPub.publish( m_scanMap.info );
        ros::spinOnce();
        r.sleep();
      }
    }

};

int main(int argc, char ** argv)
{
    ros::init(argc, argv, "scan_map");

    ScanMap scanMapObject ;
    scanMapObject.setInitMap() ;

    ROS_INFO("Started Mine Map server!");

//    mm.updateMinemap() ;

    ros::spin();

    return 0;
};


