/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, UT Arlington
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of UT Arlington nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Isura Ranatunga
 *
 * mineDetector.cpp
 *  Created on: May 6, 2014
 */

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include <metal_detector_msgs/Coil.h>

#include <Eigen/Core>

#include <mine_detector/CoilDataProcess.h>

class MetalDetector
{
public:
    MetalDetector() : m_tf(),  m_targetFrame("minefield")
    {
        m_mdSub.subscribe(m_node, "coils", 1000);
        m_tfFilter = new tf::MessageFilter<metal_detector_msgs::Coil>(m_mdSub, m_tf, m_targetFrame, 50);
        m_tfFilter->registerCallback( boost::bind(&MetalDetector::msgCallback, this, _1) );
        m_pubMineLoc = m_node.advertise<geometry_msgs::PoseStamped>("/HRATC_FW/set_mine", 1);

        double leftCoilDetectionThresh   = 0.0 ;
        double middleCoilDetectionThresh = 0.0 ;
        double rightCoilDetectionThresh  = 0.0 ;

        m_node.param( "leftDetectionThresh"  , leftCoilDetectionThresh   , 0.0 );
        m_node.param( "middleDetectionThresh", middleCoilDetectionThresh , 0.0 );
        m_node.param( "rightDetectionThresh" , rightCoilDetectionThresh  , 0.0 );

        m_mineDataProcessor.setThreshold( leftCoilDetectionThresh, middleCoilDetectionThresh, rightCoilDetectionThresh ) ;
    }

private:

    message_filters::Subscriber<metal_detector_msgs::Coil> m_mdSub    ;
    tf::TransformListener                                  m_tf       ;
    tf::MessageFilter<metal_detector_msgs::Coil> *         m_tfFilter ;

    ros::NodeHandle            m_node              ;
    ros::Publisher             m_pubMineLoc        ;

    std::string                m_targetFrame       ;
    geometry_msgs::PoseStamped m_minePose          ;

    MineDetectedStruct         m_mineDetected      ;
    CoilDataProcess            m_mineDataProcessor ;

    void setMineLoc( SensorCoilStruct & sensorCoil );

    //  Callback to register with tf::MessageFilter to be called when transforms are available
    void msgCallback(const boost::shared_ptr<const metal_detector_msgs::Coil>& coilPtr)
    {
        geometry_msgs::PointStamped pointIn                  ;
        pointIn.header.frame_id   = coilPtr->header.frame_id ;
        pointIn.header.stamp      = coilPtr->header.stamp    ;
        pointIn.point.x           = 0.0                      ;
        pointIn.point.y           = 0.0                      ;
        pointIn.point.z           = 0.0                      ;

        geometry_msgs::PointStamped point_out;

        try
        {
          m_tf.transformPoint( m_targetFrame, pointIn, point_out ) ;

          m_mineDataProcessor.readCoil( point_out, coilPtr, m_mineDetected ) ;

          if( m_mineDetected.m_detected )
          {
              ROS_WARN_STREAM( "Mine detected!" << " | "  << m_mineDetected.m_location.pose.position.x << " "
                                                          << m_mineDetected.m_location.pose.position.y << " "
                                                          << m_mineDetected.m_location.pose.position.z );

              // TODO Publish to mineDetectorNode...
              m_pubMineLoc.publish( m_mineDetected.m_location );

              m_mineDetected.m_detected = false ;
              m_mineDetected.m_location.pose.position.x = 0 ;
              m_mineDetected.m_location.pose.position.y = 0 ;
              m_mineDetected.m_location.pose.position.z = 0 ;
          }

          // TODO logic to reset mineDetected and store prev detected locations
          //

     }catch (tf::TransformException &ex)
     {
         ROS_WARN("Failure %s\n", ex.what());
     }
    }

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

void MetalDetector::setMineLoc( SensorCoilStruct & sensorCoil )
{
    geometry_msgs::PoseStamped minePose                                   ;

    minePose.header.stamp     = ros::Time::now()                          ;
    minePose.header.frame_id  = sensorCoil.m_pointIn.header.frame_id      ;
    minePose.pose.position.x  = sensorCoil.m_pointIn.point.x              ;
    minePose.pose.position.y  = sensorCoil.m_pointIn.point.y              ;
    minePose.pose.position.z  = sensorCoil.m_pointIn.point.z              ;
    minePose.pose.orientation = tf::createQuaternionMsgFromYaw(0.0)    ;

    ROS_INFO( "Setting a mine on x:%lf y:%lf", minePose.pose.position.x, minePose.pose.position.y );

    m_pubMineLoc.publish( minePose );
}

int main(int argc, char ** argv)
{
    ros::init(argc, argv, "read_metal_detector");

    MetalDetector metalDetectorObject ;

    ros::spin();

    return 0;
};

