/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, UT Arlington
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of UT Arlington nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Isura Ranatunga
 *
 * mineMap.cpp
 *  Created on: May 7, 2014
 */

#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/GetMap.h>

#include <Eigen/Core>

class MineMap
{
public:
  MineMap() : m_targetFrame("/minefield")
    {
      m_mineMapPub      = m_node.advertise<nav_msgs::OccupancyGrid> ("map", 1, false);
      m_mineMapMetaPub  = m_node.advertise<nav_msgs::MapMetaData>("map_metadata", 1, true);
      m_mineDetectedSub = m_node.subscribe("/HRATC_FW/set_mine", 1, &MineMap::mineDetectedCb, this);

      m_mapService      = m_node.advertiseService("static_map", &MineMap::mapCallback, this);

      m_minefieldDimension = m_node.subscribe("/HRATC_FW/field_dimension", 1000, &MineMap::mineDimensionCb, this);
      // FIXME figure out how to match this stuff to the real mine map size
      // Isura - 03/01/2015
      m_fieldLength     = 17.0    ; // In meters
      m_fieldBredth     = 12.0   ; // In meters
      	  m_resolution      = 0.05 ; // m/pixel
      	m_height          = m_fieldBredth/m_resolution  ;
      	m_width           = m_fieldLength/m_resolution  ;
      	m_mapInitialized = 0;
      	m_mapStartPose.position.x = - m_fieldLength/2.0 ;
		m_mapStartPose.position.y = - m_fieldBredth/2.0 ;

		m_mineMapImage    = cv::Mat::zeros( m_height, m_width, CV_8UC1);
    }
    

private:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    std::string        m_targetFrame     ;

    ros::NodeHandle    m_node            ;
    ros::Publisher     m_mineMapPub      ;
    ros::Publisher     m_mineMapMetaPub  ;
    ros::Subscriber    m_mineDetectedSub ;
    ros::ServiceServer m_mapService      ;
    ros::Subscriber    m_minefieldDimension ;

    nav_msgs::OccupancyGrid m_mineMap    ;
    cv::Mat m_mineMapImage               ;

    int    m_width                       ;
    int    m_height                      ;
    double m_resolution                  ;
    double m_fieldLength                 ;
    double m_fieldBredth                 ;
    int m_mapInitialized				 ;

    geometry_msgs::Pose m_mapStartPose   ;

    void mineDetectedCb( const geometry_msgs::PoseStamped & mineLoc )
    {
      setMineLocation( mineLoc.pose );
    }

    bool mapCallback( nav_msgs::GetMap::Request &req,
                       nav_msgs::GetMap::Response &res )
    {
      res.map = m_mineMap ;
      ROS_INFO("Sending map");

      return true;
    }

public:
    void setInitMap()
    {
		  nav_msgs::OccupancyGrid local_mineMap;

		  local_mineMap.info.resolution = m_resolution        ;
		  local_mineMap.info.width      = m_mineMapImage.cols ;
		  local_mineMap.info.height     = m_mineMapImage.rows ;

		  ROS_INFO("Initilized Mine Map!");

		  for (unsigned i=0; i < local_mineMap.info.height; ++i)
		  {
			for (unsigned j=0; j < local_mineMap.info.width; ++j)
			{
				  // TODO check this
					local_mineMap.data.push_back( m_mineMapImage.at<uchar>(i,j) );
			}
		  }

		  m_mineMap                    = local_mineMap     ;
		  m_mineMap.header.frame_id    = m_targetFrame     ;
		  m_mineMap.header.stamp       = ros::Time::now() ;
		  m_mineMap.info.map_load_time = ros::Time::now() ;
		  m_mineMap.info.origin        = m_mapStartPose    ;

    }
    void mineDimensionCb( const geometry_msgs::Point & fieldDimension )
    {
		if(m_mapInitialized == 0)
		{
			m_fieldLength = fieldDimension.x+5.0; // addition of 5m in the length and breadth to compensate for the robot being outside the arena.
			m_fieldBredth = fieldDimension.y+5.0;
			ROS_INFO("!!!!!!Map length = %g and breadth = %g", m_fieldLength, m_fieldBredth);
			m_height          = m_fieldBredth/m_resolution  ;
			m_width           = m_fieldLength/m_resolution  ;

			m_mapStartPose.position.x = - m_fieldLength/2.0 ;
			m_mapStartPose.position.y = - m_fieldBredth/2.0 ;

			m_mineMapImage    = cv::Mat::zeros( m_height, m_width, CV_8UC1);

			  nav_msgs::OccupancyGrid local_mineMap;

			  local_mineMap.info.resolution = m_resolution        ;
			  local_mineMap.info.width      = m_mineMapImage.cols ;
			  local_mineMap.info.height     = m_mineMapImage.rows ;

			  ROS_INFO("Initilized Mine Map!");

			  for (unsigned i=0; i < local_mineMap.info.height; ++i)
			  {
				for (unsigned j=0; j < local_mineMap.info.width; ++j)
				{
					  // TODO check this
						local_mineMap.data.push_back( m_mineMapImage.at<uchar>(i,j) );
				}
			  }

			  m_mineMap                    = local_mineMap     ;
			  m_mineMap.header.frame_id    = m_targetFrame     ;
			  m_mineMap.header.stamp       = ros::Time::now() ;
			  m_mineMap.info.map_load_time = ros::Time::now() ;
			  m_mineMap.info.origin        = m_mapStartPose    ;

			m_mapInitialized = 1;
		}
    }

    void setMineLocation( const geometry_msgs::Pose & mineLoc )
    {

      // Set mine
      m_mineMapImage.at<uchar>( cv::Point((int) ( mineLoc.position.x/m_resolution + m_width /2.0 ), (int) ( mineLoc.position.y/m_resolution + m_height/2.0 ) ) ) = 100 ;

      nav_msgs::OccupancyGrid local_mineMap;

      local_mineMap.info.resolution = m_resolution        ;
      local_mineMap.info.width      = m_mineMapImage.cols ;
      local_mineMap.info.height     = m_mineMapImage.rows ;

      // TODO Better way to do this
      // TODO rows/cols may be flipped
      for (unsigned i=0; i < local_mineMap.info.height; ++i)
      {
        for (unsigned j=0; j < local_mineMap.info.width; ++j)
        {
          // TODO check this
            local_mineMap.data.push_back( m_mineMapImage.at<uchar>(i,j) );
        }
      }

      m_mineMap                    = local_mineMap     ;
      m_mineMap.header.frame_id    = m_targetFrame     ;
      m_mineMap.header.stamp       = ros::Time::now()  ;
      m_mineMap.info.map_load_time = ros::Time::now()  ;
      m_mineMap.info.origin        = m_mapStartPose    ;
    }

    void updateMinemap()
    {
      ros::Rate r(50);
      while ( m_node.ok())
      {
        m_mineMapPub.publish( m_mineMap );
        m_mineMapMetaPub.publish( m_mineMap.info );
        ros::spinOnce();
        r.sleep();
      }
    }

};

int main(int argc, char ** argv)
{
    ros::init(argc, argv, "mine_map");

    MineMap mm ;
    mm.setInitMap() ;

    ROS_INFO("Started Mine Map server!");

    mm.updateMinemap() ;

    return 0;
};


