/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, UT Arlington
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of UT Arlington nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/**
*
* joyTeleop: creates a ROS node that allows the control of the husky with a joystick.
* 
*
* This node listens to a /joy topic and sends messages to the /cmd_vel topic.
* Arm control is currently unimplemented.
*
* @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2012.
* @contact isura.ranatunga@mavs.uta.edu
* @created 11/28/2012
* @modified 11/28/2012
*
*/

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>

class JoyTeleop
{
 public:
	  JoyTeleop()
	  {
	    // create ROS topics
	    cmd_vel = node.advertise<geometry_msgs::Twist>("cmd_vel", 10);
	    joy_sub = node.subscribe<sensor_msgs::Joy>("joy", 10, &JoyTeleop::joy_callback, this);
	  
	    ROS_INFO("Husky Joystick Teleop node Started!");
	  }

 private:
	  void joy_callback(const sensor_msgs::Joy::ConstPtr& joy)
	  {
	    // create twist msg
	    geometry_msgs::Twist twist;
	    // linear
	    twist.linear.x = joy->axes.at(1);
	    twist.linear.y = joy->axes.at(0);
	    twist.linear.z = 0;
	    // angular
	    twist.angular.x = 0;
	    twist.angular.y = 0;
	    twist.angular.z = joy->axes.at(2);
	    // send twist
	    cmd_vel.publish(twist);
	  }

  ros::NodeHandle node;
  ros::Publisher cmd_vel;
  ros::Subscriber joy_sub;

};

int main(int argc, char **argv)
{
  // start joyTeleop node
  ros::init(argc, argv, "joyTeleop");
  JoyTeleop teleop;
  ros::spin();
}
