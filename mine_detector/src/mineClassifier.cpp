#include <mine_detector/MineClassifier.h>

int main(int argc, char* argv[])
{
    string data(argv[1]);

    MineClassifier mClassifier(data);

    // Now let's try this decision_function on some samples we haven't seen before.
    MineClassifier::sample_type sample;

/*sample(0) = -6931    ;
sample(1) = -14255   ;
sample(2) =  2041    ;*/
/*sample(0) =  946     ;
sample(1) = -11935   ;
sample(2) =  3391.2  ;*/
sample(0) = -7996    ;
sample(1) = -18516   ;
sample(2) =  2687.3  ;

    cout << "This is a +1 class example, the classifier output is " << mClassifier.classify(sample) << endl;

/*sample(0) = -5711    ;
sample(1) = -16019   ;
sample(2) =  2897    ;*/
/*sample(0) = -4672    ;
sample(1) = -20802   ;
sample(2) =  4582.39 ;*/
sample(0) = -5368    ;
sample(1) = -13974   ;
sample(2) =  2333.60 ;
    cout << "This is a -1 class example, the classifier output is " << mClassifier.classify(sample) << endl;

/*lsample(0) = -8974    ;
sample(1) = -21175   ;
sample(2) =  3561    ;*/
/*sample(0) = -4184    ;
sample(1) = -23471   ;
sample(2) =  5096.2  ;*/
sample(0) = -7884    ;
sample(1) = -17748   ;
sample(2) =  2822.5  ;
    cout << "This is a -1 class example, the classifier output is " << mClassifier.classify(sample) << endl;

}


