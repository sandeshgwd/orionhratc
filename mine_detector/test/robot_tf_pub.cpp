/*
 * husky tf publisher odom base_footprint
 * Author: Sandesh Gowda
 * Created:5/20/2015
 */

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_broadcaster.h>

class Husky_base_tf
{
public:
	Husky_base_tf(ros::NodeHandle &node);
private:
	void husky_tf_sub(const nav_msgs::Odometry::ConstPtr & odom);

	ros::Subscriber huskyOdom_sub ;

	tf::TransformBroadcaster odom_broadcaster ;

	ros::Time currTime ;

};
Husky_base_tf::Husky_base_tf(ros::NodeHandle &node )
{
	huskyOdom_sub =  node.subscribe<nav_msgs::Odometry>("odom", 1000, &Husky_base_tf::husky_tf_sub, this);
}

void Husky_base_tf::husky_tf_sub(const nav_msgs::Odometry::ConstPtr & odom)
{
	currTime		= ros::Time::now();

	// Step 1: Publish the transform over tf
	geometry_msgs::TransformStamped odom_trans ;
	odom_trans.header.stamp     = currTime ;
	odom_trans.header.frame_id  = "odom"       ;
	odom_trans.child_frame_id   = "base_footprint"  ;

	odom_trans.transform.translation.x = odom->pose.pose.position.x     ;
	odom_trans.transform.translation.y = odom->pose.pose.position.y     ;
	odom_trans.transform.translation.z = 0.0   ;
	odom_trans.transform.rotation = odom->pose.pose.orientation  ;

	//send the transform
	odom_broadcaster.sendTransform(odom_trans) ;
}

//Main Function Call
int main(int argc, char **argv)
{
	ros::init(argc, argv, "husky_odom_launcher");
	ros::NodeHandle n;

	Husky_base_tf husky_odom(n);

	ros::spin();

	return 0;
}
