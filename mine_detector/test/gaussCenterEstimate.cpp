/*
 * Sandesh Gowda
 * 03/10/2015
 * Test run to estimate the center of the Gaussian Field
 *
 * Matlab code to get the Gaussian plot
resolution = 0.01;
column = 0:resolution:10; % x - axis
row = 0:resolution:10; % y - axis
[X, Y] = meshgrid(column, row);
amplitude = 1;
sigma_x = 0.5;
sigma_y = 0.5;
a = 1/(2*sigma_x^2) ;
b = 1/(2*sigma_y^2) ;
mines   = [5,2.5]';
mineMap = 0;
mineMap = mineMap + amplitude*exp( - ((a*(X-mines(1,i)).^2) + (b*(Y-mines(2,i)).^2))) ;
surf(mineMap)
 */

#include <ros/ros.h>
#include <math.h>
#include <nlopt.h>
#include <iostream>
#include <fstream>
#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Geometry>
#include <vector>
#include <string>
#include <mine_detector/CenterEstimation.h>
/*
typedef struct {
    double a, b, c;
} my_constraint_data;
*/

//typedef std::vector< Eigen::Vector4d,Eigen::aligned_allocator<Eigen::Vector3d> > eigen4D;

//int nIterations = 0;
/*
//contrains in the form of 0>=f(x,y)
double myconstraint(unsigned n, const double *x, double *grad, void *data)
{
    my_constraint_data *d = (my_constraint_data *) data ;
    double a = d->a, b = d->b, c = d->c				    ;
    if (grad) {
        grad[0] = b              ;
        grad[1] = c            ;
    }
    return (a + b*x[0] + c*x[1]) ;
}

double myfunc(unsigned n, const double *x, double *grad, void* my_func_data)
{
	eigen3Darray* mData = (eigen3Darray*) my_func_data;
	double minValue = 0.0                           ;
	double sigma = 0.5                              ;
//	double sigma = 0.14                              ;
	double a = 1/( 2 *( pow(sigma,2) ) )            ;
	double amplitude = 1;//96250;
	double data[9][3]={ { 4.64, 2.04, 0.4889 } ,
			              { 4.86, 2.02, 0.5914 } ,
			              { 5.10, 1.98, 0.561  } ,
			              { 4.38, 2.40, 0.4413 } ,
			              { 4.78, 2.28, 0.8093 } ,
			              { 5.09, 2.21, 0.8246 } ,
			              { 4.99, 2.59, 0.9865 } ,
			              { 5.70, 2.41, 0.3782 } ,
			              { 5.20, 2.25, 0.8127 }  } ;
	double data[9][3] = { { 5.45, 2.85, 71110 } ,
						  { 5.35, 2.80, 48430 } ,
						  { 5.65, 2.75, 52730 } ,
						  { 5.35, 2.90, 45040 } ,
						  { 5.45, 2.75, 56610 } ,
						  { 5.50, 2.80, 80870 } ,
						  { 5.50, 2.75, 62420 } ,
						  { 5.65, 2.70, 39650 } ,
						  { 5.40, 2.75, 48990 }  } ;
	++nIterations ;

	for (int i = 0; i < mData->size();i++)
	{
		minValue = minValue + fabs( mData->at(i)(2) - (x[2]*exp( -a * ( pow(mData->at(i)(0)-x[0],2) + pow(mData->at(i)(1)-x[1],2)) ) ) );
	}
	return minValue;
}*/

int main( int argc, char** argv )
{
    ros::init( argc, argv, "Estimating_Gaussian_Center" ) ;

//    std::ofstream m_outputFile                            ;
//    m_outputFile.open( "estimatingGaussianCenter.txt" )   ;
//	m_outputFile.precision(12)                            ;

	// HEADER
	/*m_outputFile       << "sigma = 0.5 & data[9][3] = { "
						  "{4.64, 2.04, 0.4889}, {4.86, 2.02, 0.5914}, {5.10, 1.98, 0.561}, "
						  "{4.38, 2.40, 0.4413}, {4.78, 2.28, 0.8093}, {5.09, 2.21, 0.8246}, "
						  "{4.99, 2.59, 0.9865}, {5.70, 2.41, 0.3782}, {5.20, 2.25, 0.8127} };" << std::endl;
	m_outputFile	   << "x0"               << ","  << "y0"                << ","
					   << "center_X"         << ","  << "center_Y"          << ","
					   << "estimated_X"      << ","  << "estimated_Y"       << ","
					   << "distFromCenter"   << ","  << "errorEstimation"   << ","
					   << "Iterations"       << ","  << "GaussianValue"    << std::endl;*/

	/*double center[2] = { 5.0, 2.5 } ;
	double x1        =   4.0        ;//x>4.5
	double x2        =   6.0        ;//x<5.5
	double y1        =   1.5        ;//y>2.0
	double y2        =   3.5        ;//y<3.0
//	double z1		 =	 0.5		;
//	double z2		 =	 1.5		;
	double center[2] = { 5.55, 2.85 } ;
	double x1        =   5.2       ;//x>4.5
	double x2        =   5.9        ;//x<5.5
	double y1        =   2.3        ;//y>2.0
	double y2        =   3.2        ;//y<3.0
//	double z1        =   0.0        ;//z>2.0
//	double z2        =   100000     ;//z<3.0


	double lb[2] = { -10000.0, 0 }              ;  lower bounds
	nlopt_opt opt                               ;
	opt = nlopt_create(NLOPT_LN_BOBYQA, 2)      ;  algorithm and dimensionality
	nlopt_set_lower_bounds(opt, lb)             ;*/
	eigen4Darray m_data      ;
	m_data.push_back( Eigen::Vector4d( 5.45, 2.85, 71110 , 0.0) );
	m_data.push_back( Eigen::Vector4d( 5.35, 2.80, 48430 , 0.0) );
	m_data.push_back( Eigen::Vector4d( 5.65, 2.75, 52730 , 0.0) );
	m_data.push_back( Eigen::Vector4d( 5.35, 2.90, 45040 , 0.0) );
	m_data.push_back( Eigen::Vector4d( 5.45, 2.75, 56610 , 0.0) );
	m_data.push_back( Eigen::Vector4d( 5.50, 2.80, 80870 , 0.0) );
	m_data.push_back( Eigen::Vector4d( 5.50, 2.75, 62420 , 0.0) );
	m_data.push_back( Eigen::Vector4d( 5.65, 2.70, 39650 , 0.0) );
	m_data.push_back( Eigen::Vector4d( 5.40, 2.75, 48990 , 0.0) );
	Eigen::Vector2d center = centerEstimation(m_data);
	ROS_INFO_STREAM("Centers arew:: " << center);
	/*nlopt_set_min_objective(opt, myfunc, &m_data)  ;

	// constrains
	my_constraint_data data[4] = { {x1,-1,0}, {-x2,1,0}, {y1,0,-1}, {-y2,0,1} };
//	my_constraint_data data[6] = { {x1,-1,0,0}, {-x2,1,0,0}, {y1,0,-1,0}, {-y2,0,1,0}, {z1,0,0,-1}, {-z2,0,0,1} } ;

	nlopt_add_inequality_constraint(opt, myconstraint, &data[0], 1e-8)          ;
	nlopt_add_inequality_constraint(opt, myconstraint, &data[1], 1e-8)          ;
	nlopt_add_inequality_constraint(opt, myconstraint, &data[2], 1e-8)          ;
	nlopt_add_inequality_constraint(opt, myconstraint, &data[3], 1e-8)          ;
//	nlopt_add_inequality_constraint(opt, myconstraint, &data[4], 1e-8)          ;
//	nlopt_add_inequality_constraint(opt, myconstraint, &data[5], 1e-8)          ;

	nlopt_set_xtol_rel(opt, 1e-8) ;

//	double x0 = static_cast <double> (rand()) / (static_cast <double> (RAND_MAX/10)) ;//random numbers bet 0 and 10
//	double y0 = static_cast <double> (rand()) / (static_cast <double> (RAND_MAX/10)) ;
	double x[4] = { 4.5, 3.0}                                                         ;   some initial guess

	ROS_INFO("f(%g,%g)", x[0], x[1]);
	double minf;  the minimum objective value, upon return

	if (nlopt_optimize(opt, x, &minf) < 0)
	{
		ROS_INFO("nlopt failed!");
	}
	else
	{
		ROS_INFO("found minimum after %d evaluations", nIterations);
		ROS_INFO("found minimum at f(%g,%g) = %0.10g", x[0], x[1], minf);
		m_outputFile << x0                                        << ","  << y0                                          << ","
					 << center[0]                                 << ","  << center[1]                                   << ","
					 << x[0]                                      << ","  << x[1]                                        << ","
					 << sqrt( pow (x0-x[0],2) + pow (y0-x[1],2) ) << ","  << sqrt( pow (5.0-x[0],2) + pow (2.5-x[1],2) ) << ","
					 << nIterations                               << ","  << minf                                        << std::endl;
		nIterations = 0;
	}
	m_outputFile.close();*/
	return 0;
}


