// Ceres Solver - A fast non-linear least squares minimizer
// Copyright 2010, 2011, 2012 Google Inc. All rights reserved.
// http://code.google.com/p/ceres-solver/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// * Neither the name of Google Inc. nor the names of its contributors may be
//   used to endorse or promote products derived from this software without
//   specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Author: sameeragarwal@google.com (Sameer Agarwal)

#include "ceres/ceres.h"
#include "glog/logging.h"

using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;

const int kNumObservations = 3;
const double data[] = {
  4.97, 4.96, 0.6065,
  4.99, 5.02, 0.8521,
  4.97, 5.08, 0.2322
};

double sigma_x = 0.05;
double sigma_y = 0.05;
double a = 1.0/pow(2.0*sigma_x,2) ;
double b = 1.0/pow(2.0*sigma_y,2) ;

struct ExponentialResidual {
  ExponentialResidual(double x, double y, double z)
      : x_(x), y_(y), z_(z) {}

  template <typename T> bool operator()(const T* const x0, const T* const y0, T* residual) const
  {
    residual[0] = T(z_) + exp( - (   a * pow( (T(x_) - x0[0]), 2)
                                   + b * pow( (T(y_) - y0[0]), 2) ) );
    return true;
  }

 private:
  const double x_;
  const double y_;
  const double z_;
};

int main(int argc, char** argv)
{

  std::cout << "a: " << a << " b: " << b  << std::endl;

  google::InitGoogleLogging(argv[0]);

  // x0 = [4.9; 5.1];
  double x0 = 5.9;
  double y0 = 5.1;

  Problem problem;

  for (int i = 0; i < kNumObservations; ++i)
  {
    problem.AddResidualBlock( new AutoDiffCostFunction<ExponentialResidual, 1, 1, 1>( new ExponentialResidual(data[3 * i], data[3 * i + 1], data[3 * i + 2])),
                              NULL, &x0, &y0                                                                                                                   );

    std::cout << "x: " << data[3 * i] << " y: " << data[3 * i + 1] << " z: " << data[3 * i + 2] << std::endl;
  }

  Solver::Options options;
  options.max_num_iterations = 100;
  options.linear_solver_type = ceres::DENSE_QR;
  options.minimizer_progress_to_stdout = true;

  Solver::Summary summary;
  Solve(options, &problem, &summary);
  std::cout << summary.BriefReport() << "\n";
  std::cout << "Initial x0: " << 0.0 << " y0: " << 0.0 << "\n";
  std::cout << "Final   x0: " << x0 << " y0: " << y0 << "\n";
  return 0;
}
