#include <iostream>     // cout, endl
#include <fstream>      // fstream
#include <vector>
#include <string>
#include <algorithm>    // copy
#include <iterator>     // ostream_operator

#include <boost/tokenizer.hpp>

int main(int argc, char* argv[])
{
    using namespace std;
    using namespace boost;

    string data(argv[1]);

    ifstream in(data.c_str());
    if (!in.is_open()) return 1;

    typedef tokenizer< escaped_list_separator<char> > Tokenizer;

    vector< string > vec;
    string line;

    while (getline(in,line))
    {
        Tokenizer tok(line);
        vec.assign(tok.begin(),tok.end());

        if (vec.size() < 3) continue;

	cout << " # " << atof(vec[0].c_str()) << " # " << atof(vec[1].c_str()) << " # " << atof(vec[2].c_str()) ;
	
        //copy(vec.begin(), vec.end(),
        //     ostream_iterator<string>(cout, "|"));

        cout << "\n----------------------" << endl;
	getchar();
    }
}
