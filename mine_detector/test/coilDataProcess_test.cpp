/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, UT Arlington
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of UT Arlington nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Isura Ranatunga
 *
 * coilDataProcess_test.cpp
 *  Created on: May ~, 2014
 *  Modified by Sandesh
 *  Last Update on April 6, 2015
 */

#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include <metal_detector_msgs/Coil.h>

#include <mine_detector/CoilDataProcess.h>
#include <UTMConverter/UTMConverter.h>
#include <Eigen/Core>
#include <string.h>

#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/Vector3Stamped.h>

#include <iostream>
#include <fstream>

class MetalDetector
{
public:
  MetalDetector() :
      m_tf(), m_targetFrame("minefield")
  {
    m_mdSub.subscribe(m_node, "coils", 1000);
    m_tfFilter = new tf::MessageFilter<metal_detector_msgs::Coil>(m_mdSub, m_tf, m_targetFrame, 50);
    m_tfFilter->registerCallback(boost::bind(&MetalDetector::msgCallback, this, _1));

    m_outputFile.open("mineDataFile.txt");
    m_outputFile.precision(12);
    // HEADER
    m_outputFile       << "Coil ch[0]"   << "," << "Coil ch[1]"   << "," << "Coil ch[2]"   << ","
                       << "Coil zero[0]" << "," << "Coil zero[1]" << "," << "Coil zero[2]" << ","
                       << "Frame ID"     << "," << "Odom X"       << "," << "Odom Y"       << "," << "Odom Z" << std::endl;
  }

  ~MetalDetector()
  {
    m_outputFile.close();
  }

private:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  message_filters::Subscriber<metal_detector_msgs::Coil> m_mdSub;

  tf::TransformListener m_tf;
  tf::MessageFilter<metal_detector_msgs::Coil> * m_tfFilter    ;
  ros::NodeHandle m_node        ;
  std::string     m_targetFrame ;

  MineDetectedStruct mineDetected;
  CoilDataProcess mineDataProcessor;

  std::ofstream m_outputFile;

  //  Callback to register with tf::MessageFilter to be called when transforms are available
  void msgCallback(const boost::shared_ptr<const metal_detector_msgs::Coil>& coilPtr)
  {
//	  ROS_INFO("Coil Data Collection ");
    geometry_msgs::PointStamped pointIn ;
    pointIn.header.frame_id = coilPtr->header.frame_id ;

//    pointIn.header.stamp    = coilPtr->header.stamp    ;
    pointIn.point.x         = 0.0 ;
    pointIn.point.y         = 0.0 ;
    pointIn.point.z         = 0.0 ;

    geometry_msgs::PointStamped pointOut;
    geometry_msgs::PointStamped pointOdom;

    try
    {
      m_tf.transformPoint(m_targetFrame, pointIn, pointOut);
//      ROS_INFO_STREAM("Coil Data Collection:::: " << pointOut.point.x);
      // Note that z is the position of the coil, not the position of the possible metal sample!
      // ROS_INFO( "Coil %s with data ch0 %d ch1 %d ch2 %d at x %f y %f z %f", coil_ptr->header.frame_id.c_str(), coil_ptr->channel[0], coil_ptr->channel[1], coil_ptr->channel[2], point_out.point.x, point_out.point.y, point_out.point.z);

      // ROS_INFO_STREAM( "Lat/long:" << " | " << fix.latitude << " " << fix.longitude << " " << fix.altitude);

      mineDataProcessor.readCoil(pointOut, coilPtr, mineDetected) ;

      m_outputFile << coilPtr->channel[0]     << "," << coilPtr->channel[1]  << "," << coilPtr->channel[2] << ","
                   << coilPtr->zero[0]        << "," << coilPtr->zero[1]     << "," << coilPtr->zero[2]    << ","
                   << pointIn.header.frame_id << "," << pointOut.point.x     << "," << pointOut.point.y     << ","<< pointOut.point.z << std::endl ;

      // ROS_DEBUG_STREAM( "Magnetic Data" << " | " << compassData.vector.x << " " << compassData.vector.y << " " << compassData.vector.z);

      if (mineDetected.m_detected)
            {
              ROS_INFO_STREAM(
                  "Mine detected!" << " | " << mineDetected.m_location.pose.position.x << " "
                                            << mineDetected.m_location.pose.position.y << " "
                                            << mineDetected.m_location.pose.position.z ) ;

              mineDetected.m_detected                 = false ;
              mineDetected.m_location.pose.position.x = 0     ;
              mineDetected.m_location.pose.position.y = 0     ;
              mineDetected.m_location.pose.position.z = 0     ;
            }
    }
    catch (tf::TransformException &ex)
    {
      ROS_WARN("Failure %s\n", ex.what());
    }
  }

};

int main(int argc, char ** argv)
{
  ros::init(argc, argv, "read_metal_detector");

  MetalDetector metalDetectorObject ;

  ros::spin();

  return 0;
}
;

