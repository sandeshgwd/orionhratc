#include <ros/ros.h>
#include <ros/package.h>
#include <string.h>

#include <mine_detector/MineClassifier.h>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "classifier");

  ros::NodeHandle n;
  MineClassifier::
  sample_type                m_sample                ;
  MineClassifier*            m_classifier            ;

  std::string path = ros::package::getPath("mine_detector");

  m_classifier = new MineClassifier(path + "/config/simDetectorSVMLeft.dat");
  double classifier = 0.0;

  m_sample(0) = -143300 ;
  classifier = m_classifier->classify(m_sample);
  ROS_INFO_STREAM("classifier value is:: "<<classifier);
  m_sample(0) = 0 ;
  classifier = m_classifier->classify(m_sample);
  ROS_INFO_STREAM("classifier value is:: "<<classifier);
  m_sample(0) = -212800 ;
classifier = m_classifier->classify(m_sample);
ROS_INFO_STREAM("classifier value is:: "<<classifier);
m_sample(0) = -108400 ;
classifier = m_classifier->classify(m_sample);
ROS_INFO_STREAM("classifier value is:: "<<classifier);
m_sample(0) = -269500 ;
classifier = m_classifier->classify(m_sample);
ROS_INFO_STREAM("classifier value is:: "<<classifier);


m_sample(0) = -1098000 ;
classifier = m_classifier->classify(m_sample);
ROS_INFO_STREAM(" metal classifier value is:: "<<classifier);
m_sample(0) = -868000 ;
classifier = m_classifier->classify(m_sample);
ROS_INFO_STREAM("classifier value is:: "<<classifier);
m_sample(0) = -5578000 ;
classifier = m_classifier->classify(m_sample);
ROS_INFO_STREAM("classifier value is:: "<<classifier);
m_sample(0) = -7509000 ;
classifier = m_classifier->classify(m_sample);
ROS_INFO_STREAM("classifier value is:: "<<classifier);
m_sample(0) = -6512000 ;
classifier = m_classifier->classify(m_sample);
ROS_INFO_STREAM("classifier value is:: "<<classifier);
m_sample(0) = -8188000 ;
classifier = m_classifier->classify(m_sample);
ROS_INFO_STREAM("classifier value is:: "<<classifier);


  return 0;
}
