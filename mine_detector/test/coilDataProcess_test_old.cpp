/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, UT Arlington
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of UT Arlington nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Isura Ranatunga
 *
 * coilDataProcess_test.cpp
 *  Created on: May ~, 2014
 */

#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include <metal_detector_msgs/Coil.h>

#include <mine_detector/CoilDataProcess.h>
#include <UTMConverter/UTMConverter.h>
#include <Eigen/Core>
#include <string.h>

#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/Vector3Stamped.h>

#include <iostream>
#include <fstream>

class MetalDetector
{
public:
  MetalDetector() :
      m_tf(), m_targetFrame("odom")
  {
    m_mdSub.subscribe(m_node, "coils", 1000);
    m_tfFilter = new tf::MessageFilter<metal_detector_msgs::Coil>(m_mdSub, m_tf, m_targetFrame, 50);
    m_tfFilter->registerCallback(boost::bind(&MetalDetector::msgCallback, this, _1));

    m_gpsSub = m_node.subscribe("/gps/fix", 10, &MetalDetector::gpsCallback, this);
//        gpsTf_filter_ = new tf::MessageFilter<sensor_msgs::NavSatFix>(gps_sub_, tf_, target_frame_, 10) ;
//        gpsTf_filter_->registerCallback( boost::bind(&MetalDetector::gpsCallback, this, _1) ) ;

    m_compassSub = m_node.subscribe("/magnetic", 10, &MetalDetector::compassCallback,this);
    m_compassPub = m_node.advertise<geometry_msgs::PoseStamped>("magnetic_pose", 1000) ;

    m_outputFile.open("mineDataFile.txt");
    m_outputFile.precision(12);
    // HEADER
    m_outputFile       << "LeftCoil ch[0]"     << "," << "LeftCoil ch[1]"      << "," << "LeftCoil ch[2]"     << ","
                       << "LeftCoil zero[0]"   << "," << "LeftCoil zero[1]"    << "," << "LeftCoil zero[2]"   << ","
                       << "MidCoil ch[0]"      << "," << "MidCoil ch[1]"       << "," << "MidCoil ch[2]"      << ","
                       << "MidCoil zero[0]"    << "," << "MidCoil zero[1]"     << "," << "MidCoil zero[2]"    << ","
                       << "RgtCoil ch[0]"      << "," << "RgtCoil ch[1]"       << "," << "RgtCoil ch[2]"      << ","
                       << "RgtCoil zero[0]"    << "," << "RgtCoil zero[1]"     << "," << "RgtCoil zero[2]"    << ","
					   << "LeftCoil Odom X"    << "," << "LeftCoil Odom Y"     << ","
					   << "MidCoil Odom X"     << "," << "MidCoil Odom Y"      << ","
					   << "RgtCoil Odom X"     << "," << "RgtCoil Odom Y"      << ","
                       << "Coil lat"           << "," << "Coil long"           << ","
                       << "Robot lat"          << "," << "Robot long"          << ","
                       << "Coil UTM East"      << "," << "Coil UTM North"      << ","
                       << "Robot UTM East"     << "," << "Robot UTM North"     << ","
                       << "Odom X"             << "," << "Odom Y"              << std::endl;
  }

  ~MetalDetector()
  {
    m_outputFile.close();
  }

private:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  message_filters::Subscriber<metal_detector_msgs::Coil> m_mdSub;
  ros::Subscriber m_gpsSub;

  tf::TransformListener m_tf;
  tf::MessageFilter<metal_detector_msgs::Coil> * m_tfFilter    ;
  tf::MessageFilter<sensor_msgs::NavSatFix>    * m_gpsTfFilter ;
  ros::NodeHandle m_node        ;
  std::string     m_targetFrame ;

  ros::Subscriber m_compassSub ;
  ros::Publisher  m_compassPub ;

  /*ros::Subscriber coilsSub    ;
  ros::Publisher  coilsModPub ;*/

  MineDetectedStruct mineDetected;
  CoilDataProcess mineDataProcessor;

  UTMCoordinates robotUtm;
  sensor_msgs::NavSatFix robotFix;

  geometry_msgs::Vector3Stamped compassData;

  std::ofstream m_outputFile;

    /*std::string nameMetalDetector ;
    std::string nameLeftCoil      ;
    std::string nameMiddleCoil    ;
    std::string nameRightCoil     ;

   void coilsCallback( metal_detector_msgs::Coil::Ptr coil_ptr )
   {
   if( !strcmp( coil_ptr->header.frame_id.c_str(), (nameMetalDetector + nameLeftCoil).c_str()   ) )
   {
   coil_ptr->header.frame_id = nameLeftCoil ;
   }

   if( !strcmp( coil_ptr->header.frame_id.c_str(), (nameMetalDetector + nameMiddleCoil).c_str() ) )
   {
   coil_ptr->header.frame_id = nameMiddleCoil ;
   }

   if( !strcmp( coil_ptr->header.frame_id.c_str(), (nameMetalDetector + nameRightCoil).c_str()  ) )
   {
   coil_ptr->header.frame_id = nameRightCoil ;
   }

   coilsModPub.publish( coil_ptr );
   }*/

  /*
   * Save to global UTM variable
   */
  void gpsCallback(const sensor_msgs::NavSatFix & msg)
  {
    robotFix = msg;
    UTMConverter::latitudeAndLongitudeToUTMCoordinates(robotFix, robotUtm);

    //ROS_DEBUG_STREAM( "Callback lat/long!" << " | " << fix.latitude << " " << fix.longitude << " " << fix.altitude);
  }

  void compassCallback(const geometry_msgs::Vector3Stamped & msg)
  {
    compassData = msg;

    geometry_msgs::PoseStamped poseMsg ;

    poseMsg.header          = msg.header   ;
    poseMsg.pose.position.x = msg.vector.x ;
    poseMsg.pose.position.y = msg.vector.y ;
    poseMsg.pose.position.z = msg.vector.z ;

    m_compassPub.publish(poseMsg) ;

  }

  //  Callback to register with tf::MessageFilter to be called when transforms are available
  void msgCallback(const boost::shared_ptr<const metal_detector_msgs::Coil>& coilPtr)
  {
    geometry_msgs::PointStamped pointIn ;
    pointIn.header.frame_id = coilPtr->header.frame_id ;
    pointIn.header.stamp    = coilPtr->header.stamp    ;
    pointIn.point.x         = 0.0 ;
    pointIn.point.y         = 0.0 ;
    pointIn.point.z         = 0.0 ;

    geometry_msgs::PointStamped pointOut;
    geometry_msgs::PointStamped pointOdom;

    try
    {
      m_tf.transformPoint(m_targetFrame, pointIn, pointOut);
//      tf_.transformPoint(target_frame_, point_in, point_odom);

      // Note that z is the position of the coil, not the position of the possible metal sample!
      // ROS_INFO( "Coil %s with data ch0 %d ch1 %d ch2 %d at x %f y %f z %f", coil_ptr->header.frame_id.c_str(), coil_ptr->channel[0], coil_ptr->channel[1], coil_ptr->channel[2], point_out.point.x, point_out.point.y, point_out.point.z);

      UTMCoordinates coilUtm;

      coilUtm.easting    = robotUtm.easting  + pointOut.point.x;
      coilUtm.northing   = robotUtm.northing + pointOut.point.y;
      coilUtm.grid_zone  = robotUtm.grid_zone;
      coilUtm.hemisphere = robotUtm.hemisphere;

      sensor_msgs::NavSatFix fix;
      UTMConverter::UTMCoordinatesToLatitudeAndLongitude(coilUtm, fix);
      fix.altitude = fix.altitude + pointOut.point.z;
      fix.position_covariance_type = sensor_msgs::NavSatFix::COVARIANCE_TYPE_UNKNOWN;

      // ROS_INFO_STREAM( "Lat/long:" << " | " << fix.latitude << " " << fix.longitude << " " << fix.altitude);

      mineDataProcessor.readCoil(pointOut, coilPtr, mineDetected) ;

      m_outputFile << mineDataProcessor.getLeftCoil()  .m_signal.channel[0] << "," << mineDataProcessor.getLeftCoil()  .m_signal.channel[1] << "," << mineDataProcessor.getLeftCoil()  .m_signal.channel[2] << ","
                 << mineDataProcessor.getLeftCoil()  .m_signal.zero[0]    << "," << mineDataProcessor.getLeftCoil()  .m_signal.zero[1]    << "," << mineDataProcessor.getLeftCoil()  .m_signal.zero[2]    << ","
                 << mineDataProcessor.getMiddleCoil().m_signal.channel[0] << "," << mineDataProcessor.getMiddleCoil().m_signal.channel[1] << "," << mineDataProcessor.getMiddleCoil().m_signal.channel[2] << ","
                 << mineDataProcessor.getMiddleCoil().m_signal.zero[0]    << "," << mineDataProcessor.getMiddleCoil().m_signal.zero[1]    << "," << mineDataProcessor.getMiddleCoil().m_signal.zero[2]    << ","
                 << mineDataProcessor.getRightCoil() .m_signal.channel[0] << "," << mineDataProcessor.getRightCoil() .m_signal.channel[1] << "," << mineDataProcessor.getRightCoil() .m_signal.channel[2] << ","
                 << mineDataProcessor.getRightCoil() .m_signal.zero[0]    << "," << mineDataProcessor.getRightCoil() .m_signal.zero[1]    << "," << mineDataProcessor.getRightCoil() .m_signal.zero[2]    << ","
                 << fix.latitude       << "," << fix.longitude      << ","
                 << robotFix.latitude  << "," << robotFix.longitude << ","
                 << coilUtm.easting    << "," << coilUtm.northing   << ","
                 << robotUtm.easting   << "," << robotUtm.northing  << ","
                 << pointOut.point.x << "," << pointOut.point.y << std::endl ;

      // ROS_DEBUG_STREAM( "Magnetic Data" << " | " << compassData.vector.x << " " << compassData.vector.y << " " << compassData.vector.z);

      if (mineDetected.m_detected)
      {
        ROS_WARN_STREAM(
            "Mine detected!" << " | " << mineDetected.m_location.pose.position.x << " "
                                      << mineDetected.m_location.pose.position.y << " "
                                      << mineDetected.m_location.pose.position.z ) ;

        ROS_WARN_STREAM(
            "Mine detected, lat/long!" << " | " << fix.latitude  << " "
                                                << fix.longitude << " "
                                                << fix.altitude ) ;

        mineDetected.m_detected                 = false ;
        mineDetected.m_location.pose.position.x = 0     ;
        mineDetected.m_location.pose.position.y = 0     ;
        mineDetected.m_location.pose.position.z = 0     ;
      }

    }
    catch (tf::TransformException &ex)
    {
      ROS_WARN("Failure %s\n", ex.what());
    }
  }

  Eigen::Vector3d gpsLatLongAltToMinefield(Eigen::Vector3d & gpsIn, std::string frame)
  {
    geometry_msgs::PointStamped groundTruthPoseMsg;
    geometry_msgs::PointStamped gpsInMsg;

    gpsInMsg.header.frame_id = frame;
    gpsInMsg.header.stamp = ros::Time::now();

    gpsInMsg.point.x = gpsIn(0);
    gpsInMsg.point.y = gpsIn(1);
    gpsInMsg.point.z = gpsIn(2);

    tf::TransformListener listener;
    ros::Time now = ros::Time::now();
    try
    {
      ros::Time now = ros::Time::now();
      listener.waitForTransform("/minefield", "/odom", now, ros::Duration(3.0));
      listener.transformPoint("/minefield", gpsInMsg, groundTruthPoseMsg);

    }
    catch (tf::TransformException &ex)
    {
      ROS_ERROR("Failed to transform!");
    }

    Eigen::Vector3d groundTruthPose;
    groundTruthPose(0) = groundTruthPoseMsg.point.x;
    groundTruthPose(1) = groundTruthPoseMsg.point.y;
    groundTruthPose(2) = groundTruthPoseMsg.point.z;

    return groundTruthPose;
  }

  bool isEqual(geometry_msgs::PointStamped & a, geometry_msgs::PointStamped & b, double epsilon)
  {
    Eigen::Vector3d p1, p2;
    p1(0) = a.point.x;
    p2(0) = b.point.x;
    p1(1) = a.point.y;
    p2(1) = b.point.y;
    p1(2) = a.point.z;
    p2(2) = b.point.z;

    return (((abs(p1) - abs(p2)).norm() < epsilon));
  }

  void convertAllGroundTruth()
  {
//      sensor_msgs::NavSatFix fix = *msg;
//
//      UTMCoordinates utm;
//      UTMConverter::latitudeAndLongitudeToUTMCoordinates(fix, utm);

    std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> > metalLocationArrayOdom;
    std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> > metalLocationArrayMinefield;
    std::vector<std::string> metalTypeArray;

    metalTypeArray.push_back("Surrogate Mine   ");
    metalTypeArray.push_back("Coke Can         ");
    metalTypeArray.push_back("Surrogate Mine   ");
    metalTypeArray.push_back("Surrogate Mine   ");
    metalTypeArray.push_back("Surrogate Mine   ");
    metalTypeArray.push_back("Small Metal Piece");
    metalTypeArray.push_back("Surrogate Mine   ");
    metalTypeArray.push_back("Surrogate Mine   ");
    metalTypeArray.push_back("Coke Can         ");
    metalTypeArray.push_back("Surrogate Mine   ");
    metalTypeArray.push_back("Surrogate Mine   ");
    metalTypeArray.push_back("Surrogate Mine   ");
    metalTypeArray.push_back("Surrogate Mine   ");
    metalTypeArray.push_back("Large Metal Piece");
    metalTypeArray.push_back("3 Metal Screws   ");

    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184680128, -8.414337443, 119.4501));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184741307, -8.414388437, 95.7582));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184699332, -8.414384059, 99.1825));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184671115, -8.414377301, 98.9176));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184704275, -8.414372335, 109.439));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184729877, -8.414407951, 93.1824));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184682831, -8.414415405, 91.3747));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184728422, -8.414420228, 100.3748));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184704928, -8.414400085, 108.2303));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184700344, -8.414447118, 92.8744));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184729649, -8.414454803, 94.9422));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184693486, -8.414459105, 94.9186));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184719873, -8.414437708, 94.8587));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184741559, -8.41446587, 94.4049));
    metalLocationArrayOdom.push_back(Eigen::Vector3d(40.184714445, -8.414482103, 95.6064));

//      for( int i = 0; i < metalTypeArray.size(); i++ )
//      {
//        metalLocationArrayMinefield.push_back( gpsLatLongAltToMinefield( metalLocationArrayOdom[i], "odom" ) ) ;
//        ROS_INFO_STREAM( "Groundtruth: " <<  metalTypeArray[i] << " | " << metalLocationArrayMinefield[i].transpose() ) ;
//      }

  }

};

int main(int argc, char ** argv)
{
  ros::init(argc, argv, "read_metal_detector");

  MetalDetector metalDetectorObject ;

  ros::spin();

  return 0;
}
;

